const admin = require('firebase-admin')
const VerifyToken = require('../auth/VerifyToken')

module.exports = (app) => {
  app.get('/api/npcs', VerifyToken, async (req, res) => {
    const db = admin.firestore()
    console.log(`checking for NPCs matching ${req.userId}`)
    const npcMatches = await db.collection('npcs').where('createdBy', '==', req.userId).get()

    const npcs = []
    if (!npcMatches.empty) {
      npcMatches.forEach(npc => { npcs.push(npc.data()) })
    }

    res.status(200).send(npcs)
  })

  app.post('/api/npc', VerifyToken, async (req, res) => {
    const db = admin.firestore()

    console.log(`npc with id ${req.body.id}`)
    const docRef = db.collection('npcs').doc(req.body.id)
    const existingNpc = await docRef.get()

    // Make sure we're not overwriting someone elses NPC
    if (existingNpc.exists) {
      const existingNpcData = existingNpc.data()
      if (existingNpcData.createdBy !== req.userId) {
        return res.status(403).send('Cant go editting another guys NPC, bit rude')
      }
    }

    docRef.set(req.body)

    res.status(200).send({ id: req.body.id })
  })

  app.delete('/api/npc', VerifyToken, async (req, res) => {
    const { userId, body } = req

    const db = admin.firestore()
    const docRef = db.collection('npcs').doc(body.id)

    const npc = await docRef.get()
    // Make sure this user has permission to delete this NPC
    if (npc.exists) {
      if (!npc.createdBy === userId) {
        res.status(403).send('That aint ya NPC')
      } else {
        docRef.delete()
        res.status(200).send({ id: body.id })
      }
    } else {
      res.status(404).send('NPC does not exist')
    }
  })
}
