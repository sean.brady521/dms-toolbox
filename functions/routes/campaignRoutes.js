const admin = require('firebase-admin')
const VerifyToken = require('../auth/VerifyToken')

module.exports = (app) => {
  app.get('/api/campaigns', VerifyToken, async (req, res) => {
    const db = admin.firestore()
    console.log(`checking for campaigns matching ${req.userId}`)
    const campaignMatches = await db.collection('campaigns').where('createdBy', '==', req.userId).get()

    const campaigns = []
    if (!campaignMatches.empty) {
      campaignMatches.forEach(campaign => { campaigns.push(campaign.data()) })
    }

    res.status(200).send(campaigns)
  })

  app.post('/api/campaign', VerifyToken, async (req, res) => {
    const db = admin.firestore()

    console.log(`Campaign with id ${req.body.id}`)
    const docRef = db.collection('campaigns').doc(req.body.id)
    const existingCampaign = await docRef.get()

    // Make sure we're not overwriting someone elses campaign
    if (existingCampaign.exists) {
      const existingCampaignData = existingCampaign.data()
      if (existingCampaignData.createdBy !== req.userId) {
        return res.status(403).send('Cant go editting another guys campaign, bit rude')
      }
    }

    docRef.set(req.body)

    res.status(200).send({ id: req.body.id })
  })

  app.delete('/api/campaign', VerifyToken, async (req, res) => {
    const { userId, body } = req

    const db = admin.firestore()
    const docRef = db.collection('campaigns').doc(body.id)

    const campaign = await docRef.get()
    // Make sure this user has permission to delete this campaign
    if (campaign.exists) {
      if (!campaign.createdBy === userId) {
        res.status(403).send('That aint ya campaign')
      } else {
        docRef.delete()
        res.status(200).send({ id: body.id })
      }
    } else {
      res.status(404).send('Campaign does not exist')
    }
  })
}
