const admin = require('firebase-admin')
const VerifyToken = require('../auth/VerifyToken')

module.exports = (app) => {
  app.get('/api/encounter-tables', VerifyToken, async (req, res) => {
    const db = admin.firestore()
    const tableMatches = await db.collection('encounterTables').where('createdBy', '==', req.userId).get()

    const tables = []
    if (!tableMatches.empty) {
      tableMatches.forEach(table => { tables.push(table.data()) })
    }

    res.status(200).send(tables)
  })

  app.post('/api/encounter-table', VerifyToken, async (req, res) => {
    const db = admin.firestore()

    console.log(`encounter with id ${req.body.id}`)
    const docRef = db.collection('encounterTables').doc(req.body.id)
    const existingTable = await docRef.get()

    // Make sure we're not overwriting someone elses NPC
    if (existingTable.exists) {
      const existingTableData = existingTable.data()
      if (existingTableData.createdBy !== req.userId) {
        return res.status(403).send('Cant go editting another guys encounter table, bit rude')
      }
    }

    docRef.set(req.body)

    res.status(200).send({ id: req.body.id })
  })

  app.delete('/api/encounter-table', VerifyToken, async (req, res) => {
    const { userId, body } = req

    const db = admin.firestore()
    const docRef = db.collection('encounterTables').doc(body.id)

    const table = await docRef.get()
    // Make sure this user has permission to delete this NPC
    if (table.exists) {
      if (!table.createdBy === userId) {
        res.status(403).send('That aint ya Table')
      } else {
        docRef.delete()
        res.status(200).send({ id: body.id })
      }
    } else {
      res.status(404).send('Encounter table does not exist')
    }
  })
}
