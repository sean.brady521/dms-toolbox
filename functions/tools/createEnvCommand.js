const secrets = require('../.runtimeconfig.json')
const fs = require('fs')

let commandStr = 'firebase functions:config:set'

for (const [key, val] of Object.entries(secrets)) {
  if (typeof val === 'object') {
    for (const [subKey, subVal] of Object.entries(val)) {
      commandStr += ` ${key}.${subKey}="${subVal}"`
    }
  } else {
    commandStr += ` config.${key.toLowerCase()}="${val}"`
  }
}

fs.writeFileSync('./setEnv.sh', commandStr)
