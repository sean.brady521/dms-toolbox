const admin = require('firebase-admin')

async function verifyToken (req, res, next) {
  const token = req.headers.authorization
  if (!token) { return res.status(403).send({ auth: false, message: 'No token provided.' }) }

  let decodedToken
  try {
    decodedToken = await admin.auth().verifyIdToken(token)
  } catch (e) {
    return res.status(500).send({ auth: false, message: e.message })
  }

  req.userId = decodedToken.uid
  next()
}

module.exports = verifyToken
