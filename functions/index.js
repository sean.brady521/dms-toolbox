const admin = require('firebase-admin')
const functions = require('firebase-functions')

const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const npcRoutes = require('./routes/npcRoutes')
const campaignRoutes = require('./routes/campaignRoutes')
const encounterRoutes = require('./routes/encounterRoutes')

const app = express()

app.use(cors({ origin: true }))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

admin.initializeApp()

npcRoutes(app)
campaignRoutes(app)
encounterRoutes(app)

exports.app = functions.https.onRequest(app)
