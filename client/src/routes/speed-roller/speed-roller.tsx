import React, { useState } from 'react'
import styles from './speed-roller.module.scss'
import { Form, Button, Popup } from 'semantic-ui-react'
import classNames from 'classnames'
import { parseFormula } from './utils/formula-parser'
import History from './components/history'
import { HistoryItem } from './speed-roller-types'
import CheatSheet from './components/cheat-sheet'

interface SpeedRollerProps {}

const SpeedRoller = (props: SpeedRollerProps) => {
  const [formula, setFormula] = useState('')
  const [results, setResults] = useState<HistoryItem[]>([])
  const [parseError, setParseError] = useState<string | null>(null)
  const [openHistory, setOpenHistory] = useState(false)
  const [openCheatsheet, setOpenCheatsheet] = useState(false)

  const handleSubmit = () => {
    const { error, value, breakdown } = parseFormula(formula)
    if (error) {
      setParseError(error)
    } else {
      setParseError(null)
      const historyItem: HistoryItem = {
        rollString: formula,
        result: `${value || ''}`,
        resultBreakdown: breakdown
      }
      const newResults = [historyItem, ...results]
      setResults(newResults)
    }
  }

  const currResult = results.length ? results[0] : undefined

  const interceptEnterKey = (e: any) => {
    if (e.keyCode === 13) { // enter
      window.setTimeout(handleSubmit, 0)
    } 
  }

  return (
    <div className={styles.root}>
      <History 
        open={openHistory}
        history={results}
        setFormula={(formula: string) => { 
          setOpenHistory(false)
          setFormula(formula)
          handleSubmit()
        }}
        onClose={() => setOpenHistory(false)}
      />
      <CheatSheet 
        open={openCheatsheet}
        onClose={() => setOpenCheatsheet(false)}
      />
      <div className={styles.header}>
        Speed Roller
      </div>
      <div className={styles.container}>
        <div className={styles.calculator}>
          <Button 
            circular 
            icon='arrow right'
            className={styles.go}
            onClick={handleSubmit}
          />
          <Popup 
            trigger={
              <Button 
                size='tiny'
                circular 
                icon='help'
                className={styles.help}
              />
            }
          >
            {currResult?.resultBreakdown || 'Your result breakdown will appear here'}
          </Popup>
          <Form onSubmit={(handleSubmit)} className={styles.form}>
            <Form.TextArea 
              className={classNames(styles.textArea, styles.leftText)}
              onKeyDown={interceptEnterKey}
              onChange={(_, { value }) => setFormula(`${value}`.replace('\n', ''))}
              value={formula}
              rows={5}
              error={parseError}
            />
            <Form.TextArea 
              placeholder='Result'
              rows={5}
              className={classNames(styles.textArea, styles.rightText, !!(currResult?.result) && styles.disabled)}
              value={currResult?.result}
            />
          </Form>
        </div>
        <div className={styles.menus}>
          <div className={styles.actionWrap}>
            <Button 
              circular 
              size='huge'
              icon='history'
              className={styles.actionButton}
              onClick={() => setOpenHistory(true)}
            />
            <div>
              History
            </div>
          </div>
          <div className={styles.actionWrap}>
            <Button 
              circular 
              size='huge'
              icon='file alternate outline'
              className={styles.actionButton}
              onClick={() => setOpenCheatsheet(true)}
            />
            <div>
              Guide
            </div>
          </div>
        </div>
      </div>
    </div>
  )

}

export default SpeedRoller