export interface HistoryItem {
  rollString: string
  result: string
  resultBreakdown: string
}