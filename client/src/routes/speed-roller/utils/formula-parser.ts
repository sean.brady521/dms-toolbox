import { randBetween } from "utils/rand"

const diceRgx = /\d*d\d+/ig
const rollRgx = /(\d*)d(\d+)/i
const advRgx = /adv/ig
const disRgx = /dis/ig
const validSymbolRgx = /[\d+-]/ig

interface ResultWBreakdown {
  result: number | undefined
  breakdown: string
}

export function parseRoll(roll: string): ResultWBreakdown {
  let rolls = [] 
  const rollMatch = roll.match(rollRgx)

  if (!rollMatch) return { result: undefined, breakdown: '' }

  const diceCount = rollMatch[1] ? parseInt(rollMatch[1]) : 1
  const dieSize = parseInt(rollMatch[2])
  let sum = 0
  for (let i = 0; i < diceCount; i++) {
    const roll = randBetween(1, dieSize)
    rolls.push(roll)
    sum += roll
  }

  const breakdown = rolls.join(' + ')
  return { result: sum, breakdown }
}

export interface EvalResult {
  value: number | null
  breakdown: string
  error: string | null
}

export function parseFormula (formula: string): EvalResult {
  let resolvedFormula = formula
  let breakdownForumla = formula

  // Parse any dice regex
  const diceMatches = formula.match(diceRgx)
  if (diceMatches) {
    for (const dice of diceMatches) {
      const { result, breakdown } = parseRoll(dice)
      if (!result) continue
      resolvedFormula = resolvedFormula.replace(dice, `${result}`)
      breakdownForumla = breakdownForumla.replace(dice, `${dice}(${breakdown})`)
    }
  }


  // Parse advantage
  const disMatches = formula.match(disRgx) || []
  const advMatches = formula.match(advRgx) || []
  const advDisMatches = [...advMatches, ...disMatches]
  if (advDisMatches.length) {
    for (const advOrDis of advDisMatches) {
      const roll1 = randBetween(1, 20)
      const roll2 = randBetween(1, 20)
      const result = advOrDis === 'adv' 
        ?  Math.max(roll1, roll2) 
        : Math.min(roll1, roll2)
      resolvedFormula = resolvedFormula.replace(advOrDis, `${result}`)
      breakdownForumla = breakdownForumla.replace(advOrDis, `${advOrDis}(${roll1}, ${roll2})`)
    }
  }

  const validSymbolMatch = resolvedFormula.match(validSymbolRgx)
  let result
  if (validSymbolMatch) {
    const validResolvedFormula = validSymbolMatch.join('')
    const cleanFormula = resolvedFormula.replace(/\s+/g, '')
    if (cleanFormula != validResolvedFormula) { 
      result = { 
        value: null,
        breakdown: '',
        error: `invalid symbols: ${cleanFormula.replace(validSymbolRgx, '')}`
      }
    } else { 
      result = { 
        value: eval(validResolvedFormula),
        breakdown: breakdownForumla,
        error: null
      }
    }
  } else {
    result = {
      value: null,
      breakdown: '',
      error: 'no valid keywords'
    }
  }

  return result

}