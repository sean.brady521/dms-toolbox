import React from 'react'
import styles from './history.module.scss'
import { Modal, Popup, Button } from 'semantic-ui-react'
import { HistoryItem } from '../../speed-roller-types'
import { ReactComponent as DiceIcon } from 'assets/icons/dice.svg'

interface HistoryProps {
  open: boolean
  history: HistoryItem[]
  onClose: () => void
  setFormula: (formula: string) => void
}

const History = (props: HistoryProps) => {
  const {
    open,
    history,
    setFormula,
    onClose
  } = props

  return (
    <Modal 
      open={open}
      onClose={onClose}
      size='tiny'
      className={styles.modal}
    >
      <h1 className={styles.header}>
        History
      </h1>
      <div className={styles.historyItems}>
        {history.map((hItem, idx) => (
          <div
            key={idx} 
            className={styles.historyItem}
          >
            <div className={styles.leftSection}>
              <div className={styles.rollString}>
                {hItem.rollString}
              </div>
              <div className={styles.rollResult}>
                {hItem.result}
              </div>
            </div>
            <div className={styles.buttons}>
              <div className={styles.reroll}>
                <Popup 
                  content='reroll'
                  offset={-10}
                  trigger={
                    <DiceIcon 
                      onClick={() => setFormula(hItem.rollString || '')}
                      className={styles.rollButton} 
                    />
                  }
                />
              </div>
              <div className={styles.help}>
                <Popup 
                  trigger={
                    <Button 
                      size='tiny'
                      circular 
                      icon='help right'
                      className={styles.help}
                    />
                  }
                >
                  {hItem?.resultBreakdown || 'no breakdown'}
                </Popup>
              </div>
            </div>
          </div>
        ))}
        {!history.length &&
          <div>
            no history yet
          </div>
        }
      </div>
    </Modal>  
  )
}

export default History