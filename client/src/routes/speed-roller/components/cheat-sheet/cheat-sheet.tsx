import React from 'react'
import styles from './cheat-sheet.module.scss'
import { Modal } from 'semantic-ui-react'

interface CheatSheetProps {
  open: boolean
  onClose: () => void
}

const CheatSheet = (props: CheatSheetProps) => {
  const {
    open,
    onClose
  } = props

  return (
    <Modal 
      open={open}
      onClose={onClose}
      size='tiny'
      className={styles.modal}
    >
      <h1 className={styles.header}>
        Cheat Sheet
      </h1>
      <div className={styles.cheatSheetGrid}>
        <div className={styles.headerRow}>command</div>
        <div className={styles.headerRow}>description</div>
        <div> {'{n}d{n}'} </div>
        <div> roll any dice combo. e.g. d6 + 4d4 </div>
        <div> adv </div>
        <div> roll a d20 with advantage </div>
        <div> dis </div>
        <div> roll a d20 with disadvantage </div>
      </div>
    </Modal>  
  )
}

export default CheatSheet