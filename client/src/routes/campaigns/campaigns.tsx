import React, { useState } from 'react'
import { Link, Redirect } from 'react-router-dom'

import { ICampaign } from 'types/campaigns'
import { SortConfig } from 'types/common'
import styles from './campaigns.module.scss'
import CampaignForm from 'components/campaign-form'
import Dialog from 'components/dialog'
import { Button, Loader, Placeholder } from 'semantic-ui-react'


export interface CampaignsProps {
  campaigns: ICampaign[]
  loading: boolean
  loggedIn: boolean
  error: string | null
  search: string
  numPlaceholders: number
  sortConfig: SortConfig

  deleteCampaign: (id: string) => void
  createOrEditCampaign: (char: ICampaign) => Promise<string>
  generateNewCampaign: () => ICampaign | null
}

const Campaigns = (props: CampaignsProps) => {
  let { 
    campaigns,
    loading,
    loggedIn,

    createOrEditCampaign,
    deleteCampaign,
    generateNewCampaign,
  } = props

  const [openItem, setOpenItem] = useState<null | string>(null)
  const [edit, setEdit] = useState<ICampaign | null>(null)
  const [deleteConfirm, setDeleteConfirm] = useState<string | null>(null)

  if (!loggedIn) return (
    <div className={styles.pleaseLogin}>
      Please <Link to='/login'>sign in</Link> to use this feature.
    </div>
  )

  const handleDelete = (id: string) => {
    setOpenItem(null)
    setEdit(null)
    deleteCampaign(id)
    setDeleteConfirm(null)
  }

  return (
    <div className={styles.root}>
      <Dialog 
        open={!!deleteConfirm}
        message='Are you sure you want to delete this campaign?'
        primaryText='Confirm'
        onPrimary={() => deleteConfirm && handleDelete(deleteConfirm)}
        secondaryText='Cancel'
        onSecondary={() => setDeleteConfirm(null)}
        onClose={() => setDeleteConfirm(null)}
      />
      {edit && 
        <CampaignForm 
          campaign={edit} 
          open={!!openItem}
          onClose={() => {
            const underlyingNpc = campaigns.find(n => n.id === openItem)
            if (JSON.stringify(edit) !== JSON.stringify(underlyingNpc)) {
              createOrEditCampaign(edit)
            }
            setEdit(null)
            setOpenItem(null)
          }}
          onChange={(name: string, value: string) => { 
            const newEdit = { ...edit, [name]: value }
            setEdit(newEdit)
          }}
          onDelete={() => setDeleteConfirm(edit.id)}
        />
      }
      <div className={styles.container}>
        <div className={styles.header}>
          Campaigns
        </div>
          {!loading && 
            <div className={styles.campaignList}>
            {campaigns.map(camp => (
              <div 
                key={camp.id}
                onClick={() => {
                  setOpenItem(camp.id)
                  setEdit(camp)
                }}
                className={styles.listItem} 
              >
                {camp.name || 'Untitled'}
              </div>
            ))}
            <Button 
              className={styles.button}
              onClick={async () => {
                const newCampaign = generateNewCampaign()
                if (!newCampaign) return
                setOpenItem(newCampaign.id)
                setEdit(newCampaign)
                createOrEditCampaign(newCampaign)
              }} >
              Add Campaign +
            </Button>
            </div>
          }
          {loading &&
            <div>
              <Loader active className={styles.loader} />
            </div>
          }
        </div>
    </div>
  )
}

export default Campaigns