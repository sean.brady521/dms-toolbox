import React, { useEffect } from 'react'
import Campaigns, { CampaignsProps } from './campaigns'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import { ICampaign } from 'types/campaigns'
import { fetchCampaigns, postCampaign, removeCampaign } from 'store/campaigns/campaigns-actions'
import { getUserId } from 'utils/auth'
const { v4: uuidv4 } = require('uuid')

interface CampaignsContainerProps extends CampaignsProps {
  fetchCampaigns: () => void
}

export const CampaignsContainer = ({ fetchCampaigns, ...props }: CampaignsContainerProps) => {
  useEffect(() => {
    fetchCampaigns()
    window.scrollTo(0, 0)
  }, [])

  return (
    <Campaigns {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { loggedIn } = state.user
  const { campaigns, loading, error } = state.campaigns

  return {
    campaigns,
    loading,
    loggedIn,
    authenticating: state.user.loading,
    error,
    generateNewCampaign: () => {
      const uid = getUserId()
      if (!uid) return null

      return {
        id: uuidv4(),
        createdBy: uid
      }
    }
  }
}

export function mapDispatchToProps(dispatch: any) {
  return {
    fetchCampaigns: () => dispatch(fetchCampaigns()),
    createOrEditCampaign: (campaign: Partial<ICampaign>) => dispatch(postCampaign(campaign)),
    deleteCampaign: (id: string) => dispatch(removeCampaign(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CampaignsContainer)