import React, { useEffect } from 'react'
import Npcs, { NpcsProps } from './npcs'
import { connect } from 'react-redux'
import { StoreState, SortConfig } from 'types/common'
import { INpc, NpcGeneraterConfig } from 'types/npcs'
import { fetchNpcs, postNpc, removeNpc, setSearch, setSort, setSelectedCampaign } from 'store/npcs/npcs-actions'
import { selectFilteredNpcs, selectSortedNpcs } from 'store/npcs/npcs-selectors'
import { generateNewNpc } from 'utils/npc-generator/npc-generator'
import { fetchCampaigns } from 'store/campaigns/campaigns-actions'
import { getUserId } from 'utils/auth'

interface NpcsContainerProps extends NpcsProps {
  fetchNpcs: () => void
  fetchCampaigns: () => void
}

export const NpcsContainer = ({ fetchNpcs, fetchCampaigns, ...props }: NpcsContainerProps) => {
  useEffect(() => {
    fetchNpcs()
    fetchCampaigns()
    window.scrollTo(0, 0)
  }, [])

  return (
    <Npcs {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { loggedIn } = state.user
  const { npcs, loading, error, search, sortConfig, selectedCampaign: selectedCampaignId } = state.npcs
  const { campaigns } = state.campaigns
  const uid = getUserId()
  const selectedCampaign = selectedCampaignId 
    ? campaigns.find(c => c.id === selectedCampaignId) 
    : undefined


  const saveGeneratorConfig = (newConf: NpcGeneraterConfig) => {
    window.localStorage.setItem('npc_config', JSON.stringify(newConf))
  }

  const generatorConfStr = window.localStorage.getItem('npc_config')
  const generatorConfig = generatorConfStr ? JSON.parse(generatorConfStr) : {}

  const estimatedNpcCount = parseInt(window.localStorage.getItem('npc_count') || '0') 
  
  return {
    search,
    selectedCampaign,
    generatorConfig,
    sortConfig,
    npcs: selectSortedNpcs(selectFilteredNpcs(npcs, search, selectedCampaignId), sortConfig),
    numPlaceholders: estimatedNpcCount,
    campaigns,
    loading,
    loggedIn,
    authenticating: state.user.loading,
    error,
    generateNewNpc: (randomise?: boolean) => generateNewNpc(uid, selectedCampaignId, !!randomise),
    saveGeneratorConfig
  }
}

export function mapDispatchToProps(dispatch: any) {
  return {
    fetchNpcs: () => dispatch(fetchNpcs()),
    fetchCampaigns: () => dispatch(fetchCampaigns()),
    createOrEditNpc: (npc: Partial<INpc>) => dispatch(postNpc({ ...npc, modifiedAt: Date.now() })),
    deleteNpc: (id: string) => dispatch(removeNpc(id)),
    searchNpcs: (search: string) => dispatch(setSearch(search)),
    sortNpcs: (sortConfig: SortConfig) => dispatch(setSort(sortConfig)),
    setSelectedCampaign: (id: string) => {
      window.localStorage.setItem('selectedCampaign', id)
      return dispatch(setSelectedCampaign(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NpcsContainer)