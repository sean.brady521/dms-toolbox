import React, { useState } from 'react'
import { Icon, Input, Dropdown, Popup, Checkbox, Loader } from 'semantic-ui-react'
import { Link, Redirect } from 'react-router-dom'
import set from 'lodash.set'

import NpcCard from 'components/npc-card'
import NpcForm from 'components/npc-form'

import { INpc, GenerateOptions, NpcGeneraterConfig } from 'types/npcs'
import styles from './npcs.module.scss'
import NpcCardPlaceholder from 'components/npc-card-placeholder'
import { SortConfig } from 'types/common'
import { randomiseNpc, duplicateNpc, npcHasData, extractTitle } from 'utils/npc-generator/npc-generator'
import Dialog from 'components/dialog'
import { ICampaign } from 'types/campaigns'
import { useInterval } from 'utils/interval'
import stringify from 'json-stable-stringify'
import omit from 'object.omit'


export interface NpcsProps {
  npcs: INpc[]
  campaigns: ICampaign[]
  selectedCampaign?: ICampaign
  numPlaceholders: number
  loading: boolean
  generatorConfig: NpcGeneraterConfig
  loggedIn: boolean
  error: string | null
  search: string
  sortConfig: SortConfig

  deleteNpc: (id: string) => void
  searchNpcs: (search: string) => void
  sortNpcs: (sortConfig: SortConfig) => void
  createOrEditNpc: (char: INpc) => Promise<string>
  editGeneratorConfig: (newConfig: NpcGeneraterConfig) => void
  generateNewNpc: (randomise?: boolean) => INpc | null
  setSelectedCampaign: (id: string) => void
  saveGeneratorConfig: (newConf: NpcGeneraterConfig) => void
}

const Npcs = (props: NpcsProps) => {
  const {
    npcs,
    campaigns,
    numPlaceholders,
    selectedCampaign,
    generatorConfig,
    loading,
    loggedIn,
    sortConfig,
    error,

    createOrEditNpc,
    deleteNpc,
    searchNpcs,
    sortNpcs,
    generateNewNpc,
    setSelectedCampaign,
    saveGeneratorConfig
  } = props

  const [openCard, setOpenCard] = useState<null | string>(null)
  const [viewMode, setViewMode] = useState<'edit' | 'view'>('edit')
  const [edit, setEdit] = useState<INpc | null>(null)
  const [warnOverwrite, setWarnOverwrite] = useState(false)
  const [generateOptions, setGenerateOptions] = useState<GenerateOptions>({})
  const [deleteConfirm, setDeleteConfirm] = useState<string|null>(null)
  const [autorandomise, setAutorandomise] = useState<boolean>(generatorConfig.autoRandomise ? !!generatorConfig.autoRandomise : true)
  const [generateSuccessful, setGenerateSuccessful] = useState<boolean>(false)

  const saveNpc = () => {
    if (edit === null) return
    const underlyingNpc = npcs.find(n => n.id === openCard)
    if (underlyingNpc && stringify(omit(edit, ['modifiedAt'])) !== stringify(omit(underlyingNpc, ['modifiedAt']))) {
      createOrEditNpc(edit)
    }
  }

  useInterval(saveNpc, 5000)

  const sortOptions = [
    {text: 'Name', value: 'name'},
    {text: 'Modifed At', value: 'modifiedAt'},
    {text: 'Created At', value: 'createdAt'},
    {text: 'Context', value: 'context'}
  ]

  const handleDelete = (id: string) => {
    deleteNpc(id)
    setOpenCard(null)
    setDeleteConfirm(null)
  }

  const handleGenerate = (overwrite: boolean = false) => {
    if (!edit) return
    const newEdit = randomiseNpc(edit, generateOptions, overwrite)
    setEdit(newEdit)
    setWarnOverwrite(false)
    setGenerateSuccessful(true)
  }

  const campaignOps = [ 
    { text: 'all', value: 'all' },
    ...campaigns.map(c => ({ text: c.name || 'Untitled', value: c.id }))
  ]

  return (
    <div className={styles.root}>
      <Dialog 
        open={!!deleteConfirm}
        message='Are you sure you want to delete this NPC?'
        primaryText='Confirm'
        onPrimary={() => deleteConfirm && handleDelete(deleteConfirm)}
        secondaryText='Cancel'
        onSecondary={() => setDeleteConfirm(null)}
        onClose={() => setDeleteConfirm(null)}
      />
      <Dialog 
        open={warnOverwrite}
        message='What about your existing information?'
        primaryText='Fill Blanks'
        onPrimary={() => handleGenerate()}
        secondaryText='Overwrite'
        secondaryIsPrimary
        onSecondary={() => handleGenerate(true)}
        onClose={() => setWarnOverwrite(false)}
      />
      {edit && 
        <NpcForm 
          npc={edit}
          campaigns={campaigns}
          open={!!openCard} 
          generateSuccessful={generateSuccessful}
          readOnly={viewMode === 'view'}
          onClose={() => { 
            saveNpc()
            setOpenCard(null)
            setViewMode('edit')
            setEdit(null)
            setGenerateSuccessful(false)
          }}
          onChange={(name: string, value: string) => { 
            const newEdit = JSON.parse(JSON.stringify(edit))
            set(newEdit, name, value)
            setEdit(newEdit)
          }}
          onGenerate={async (options: GenerateOptions = {}) => {
            setGenerateOptions(options)
            if (npcHasData(edit)) {
              setWarnOverwrite(true)
            } else {
              setGenerateSuccessful(true)
              const newEdit = randomiseNpc(edit, options)
              setEdit(newEdit)
            }
          }}
          onDelete={() => openCard && setDeleteConfirm(openCard)}
        />}
      <div className={styles.container}>
        <div className={styles.headerWrapper}>
          <div className={styles.header}>
            Non-player Characters
          </div>
          <Popup
            trigger={(<Icon size='large' className={styles.settingsCog} name='cog' />)} 
            on='click'
            basic
            position='bottom center'
          >
            <Checkbox 
              checked={autorandomise}
              label='randomise on creation' 
              onChange={(_, { checked }) =>{
                saveGeneratorConfig({...generatorConfig, autoRandomise: checked})
                setAutorandomise(checked || false)
              }}
            />
          </Popup>
        </div>
        <div className={styles.searchAndSort}>
          <Input 
            className={styles.search}
            icon='search' 
            onChange={(_, { value }) => searchNpcs(value)}
            placeholder='Search by name or context...'
          />
          <div className={styles.sort}>
            <Dropdown 
              icon={null} 
              value={sortConfig.field} 
              className={styles.sortDropdown}
              options={sortOptions}
              onChange={(_, {value}) => sortNpcs({...sortConfig, field: `${value}`})}
            />
            <Icon 
              name={sortConfig.direction === 'desc' ? 'arrow down' : 'arrow up'}
              className={styles.sortIcon}
              onClick={() => { 
                const direction = sortConfig.direction === 'desc' ? 'asc' : 'desc'
                sortNpcs({ ...sortConfig, direction })
              }}
            />
          </div>
        </div>
        <div className={styles.selectedCampaign}>
          Showing NPC's from{' '}
          <Dropdown 
            labeled 
            inline
            value={selectedCampaign?.id || 'all'}
            options={campaignOps}
            onChange={(_, { value }) => {
              setSelectedCampaign((value || '') as string)
            }}
          />
          {(!selectedCampaign || selectedCampaign.id === 'all') && 
            ' campaigns'
          }
        </div>
        <div className={styles.npcCards}>
        {!loading &&
          <>
            {npcs.map((npc, idx) => (
              <NpcCard 
                imageUrl={npc.imageUrl}
                name={npc.name}
                context={npc.context}
                title={extractTitle(npc)}
                key={idx}
                className={styles.card}
                onEdit={() => { 
                  setOpenCard(npc.id)
                  setEdit(npc)
                  setViewMode('edit')
                }}
                onView={() => { 
                  setOpenCard(npc.id)
                  setEdit(npc)
                  setViewMode('view')
                }}
                onDuplicate={() => {
                  const newNpc = duplicateNpc(npc)
                  createOrEditNpc(newNpc)
                }}
                onDelete={() => setDeleteConfirm(npc.id)}
              />
            ))}
          </>}
          {(loading && !!numPlaceholders) &&
            [...Array(numPlaceholders)].map((_, idx) => (
              <NpcCard className={styles.card} placeholder key={idx} />
            ))
          }
          <NpcCardPlaceholder 
            className={styles.card}
            onClick={() => {
              const newNpc = generateNewNpc(autorandomise)
              if (!newNpc) return
              setOpenCard(newNpc.id)
              setEdit(newNpc)
              createOrEditNpc(newNpc)
            }} 
          />
        </div>
        {(loading && !numPlaceholders) &&
          <div>
            <Loader active className={styles.loader} />
          </div>
        }
      </div>
      {!loggedIn && 
      <div className={styles.pleaseLogin}>
        <Link to='/login'>Sign in</Link> to automatically save your NPC's.
      </div>}
    </div>
  )
}

export default Npcs