import React, { useState } from 'react'
import styles from './home.module.scss'
import DisplayCard from 'components/display-card'
import { Redirect } from 'react-router-dom'
import npcBg from 'assets/images/npcs-bg.jpg'
import campaignBg from 'assets/images/campaign-bg.jpg'
import encounterBg from 'assets/images/encounter.jpg'
import speedRollerBg from 'assets/images/speed-roller.jpg'
import fuzzyCalcBg from 'assets/images/fuzzy-calc.jpg'

type Page = string | null

const Home = () => {
  const [redirectTo, setRedirectTo] = useState<Page>(null)
  return (
    <div className={styles.root}>
      <div className={styles.backgroundImage} />
      <div className={styles.imageText}>
        Dungeon Master's
        <br />
        Toolbox
      </div>
      <div className={styles.displayCards}>
        <DisplayCard
          className={styles.displayCard}
          imageSrc={npcBg}
          title='Randomly Generate NPCs'
          body='You can randomly generate and keep track of the Non-Player Characters in your campaign'
          moreText='Try it now'
          onMore={() => setRedirectTo('npcs')}
        />
        <DisplayCard
          className={styles.displayCard}
          imageSrc={campaignBg}
          title='Manage your campaigns'
          body={`Add your campaigns and filter your content by the campaigns they're relevant to`}
          moreText='Try it now'
          onMore={() => setRedirectTo('campaigns')}
        />
        <DisplayCard
          className={styles.displayCard}
          imageSrc={speedRollerBg}
          size='small'
          title='Speed through dice rolls'
          body='Keep those encounters fast paced and fun. Simulate large dice rolls by entering rolls in shorthand.'
          moreText='Try it now'
          onMore={() => setRedirectTo('speed-roller')}
        />
        <DisplayCard
          className={styles.displayCard}
          imageSrc={encounterBg}
          size='small'
          title='Randomly Generate Encounters'
          body='Generate a single random encounter or create your own encounter table out of generated or manual encounters.'
          moreText='Try it now'
          onMore={() => setRedirectTo('encounters')}
        />
        <DisplayCard
          className={styles.displayCard}
          imageSrc={fuzzyCalcBg}
          size='small'
          title='Calculate Encounter Variables'
          body='Use the fuzzy encounter calculator to determine the missing variable in your encounter.'
          moreText='Try it now'
          onMore={() => setRedirectTo('encounters?fuzzyCalc=open')}
        />
      </div>
      {redirectTo && <Redirect to={`/${redirectTo}`} />}
    </div>
  )
}

export default Home
