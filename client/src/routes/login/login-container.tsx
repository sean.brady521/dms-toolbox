import React from 'react'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import Login, { LoginProps } from './login'
import { setError, setLoading } from 'store/user/user-actions'
import { sendSignOnEmail } from 'utils/auth'

export function mapStateToProps (state: StoreState) {
  const { loading, loggedIn, error } = state.user

  return {
    loggedIn,
    loading,
    error,
  }
}

export function mapDispatchToProps (dispatch: any) {

  const handleLogin = async (email: string) => {
    let success
    try {
      await sendSignOnEmail(email)
      window.localStorage.setItem('emailForSignIn', email)
      success = true
    } catch (e) {
      setError(`Something went wrong, please try again (code: ${e.status})`)
      console.error(e)
      success = false
    }
    return success
  }

  return {
    handleLogin
  }
}

export const LoginContainer = (props: LoginProps) => {
  return (
    <Login {...props} />
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)
