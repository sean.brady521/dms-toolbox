import React, { useState } from 'react'
import { Button, Form, Header, Segment } from 'semantic-ui-react'
import { Redirect } from 'react-router-dom'
import styles from './login.module.scss'

export interface LoginProps {
  handleLogin: (username: string) => Promise<boolean>
  loggedIn: boolean
  error: string | null
}

export default function Login (props: LoginProps) {
  const { handleLogin, loggedIn, error } = props

  const [email, setEmail] = useState('')
  const [emailSent, setEmailSent] = useState<boolean>(false)
  const [loginPending, setLoginPending] = useState<boolean>(false)

  return (
    <div className={styles.root}>
          <div className={styles.loginWrapper}>
            <Header as='h2' color='black' textAlign='center'>
              Passwordless sign in
            </Header>
            <Form size='large' className={styles.form}>
              <Segment className={styles.segment}>
                <div className={styles.error}>
                  {error && error}
                </div>
                {!emailSent && (
                  <>
                    <Form.Input
                      fluid
                      icon='user'
                      name='email'
                      iconPosition='left'
                      placeholder='E-mail address'
                      value={email}
                      onChange={(event) => setEmail(event.target.value)}
                    />
                    <Button
                      color='black'
                      fluid
                      loading={loginPending}
                      size='large'
                      onClick={async () => { 
                        setLoginPending(true)
                        const success = await handleLogin(email)
                        setLoginPending(false)
                        setEmailSent(success)
                      }}
                    >
                      Login
                    </Button>
                  </>
                )}
                {emailSent && (
                  <div className={styles.confirmation}>
                    A secure sign on link has been sent to your email
                  </div>
                )}
              </Segment>
            </Form>
          </div>
      {loggedIn && <Redirect to='/home' />}
  </div>
  )
}
