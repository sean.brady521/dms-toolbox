import React, { useState } from 'react'
import classNames from 'classnames'

import { IEncounter, EncounterTable, EncounterRow, EncounterConfig } from 'types/encounters'
import styles from './encounters.module.scss'
import { Button, Dropdown, Icon, Popup } from 'semantic-ui-react'
import EncounterForm from './components/encounter-form'
import { Likihood } from 'constants/enums'
import { generateEncounter, generateEncounterRow } from 'utils/encounter-generator/generator'
import ConfigPopup from 'components/config-popup'
import { selectFromTable } from 'utils/encounter-generator/tables'
import omit from 'object.omit'
import stringify from 'json-stable-stringify'
import { useInterval } from 'utils/interval'
import FuzzyCalc from './components/fuzzy-calc'
import { fuzzyEncounterCalculate } from 'utils/fuzzy-calc'
import { Link, Redirect } from 'react-router-dom'

const NO_ENCOUNTER: IEncounter = {
  creatures: 'No Creatures',
  personality: 'No Personality',
  relationship: 'No Relationships'
}

export interface EncountersProps {
  tables: EncounterTable[]
  loading: boolean
  loggedIn: boolean
  defaultEncounterConfig: EncounterConfig
  defaultCalcOpen?: boolean
  error: string | null

  deleteEncounterTable: (id: string) => void
  saveLastEncounterConf: (conf: EncounterConfig) => void
  generateNewTable: () => EncounterTable | null
  createOrEditEncounterTable: (table: Partial<EncounterTable>) => Promise<string>
}

const Encounters = (props: EncountersProps) => {
  const {
    tables = [],
    loggedIn,
    defaultEncounterConfig,
    defaultCalcOpen = false,

    createOrEditEncounterTable,
    generateNewTable,
    saveLastEncounterConf,
    deleteEncounterTable,
  } = props

  const [showTable, setShowTable] = useState<null | string>(null)
  const [tableEdit, setTableEdit] = useState<null | EncounterTable>(null)
  const [currEncounter, setCurrEncounter] = useState<IEncounter>(NO_ENCOUNTER)
  const [confPopupOpen, setConfPopupOpen] = useState<boolean>(false)
  const [calcPopupOpen, setCalcPopupOpen] = useState<boolean>(defaultCalcOpen)

  const tableOps = [
    ...tables.map(table => ({ text: table.name, value: table.id })), 
  ]

  const saveEncounterTable = () => {
    if (tableEdit === null) return
    const underlyingEncounterTable = tables.find(t => t.id === showTable)
    if (underlyingEncounterTable && stringify(omit(tableEdit, ['modifiedAt'])) !== stringify(omit(underlyingEncounterTable, ['modifiedAt']))) {
      createOrEditEncounterTable(tableEdit)
    }
  }

  useInterval(saveEncounterTable, 5000)

  return (
    <div className={styles.root}>
      {tableEdit && 
        <EncounterForm 
          table={tableEdit}
          open={!!showTable}
          defaultEncounterConfig={defaultEncounterConfig}
          onChangeName={(name: string) => {
            setTableEdit({ ...tableEdit, name })
          }}
          onAddTableRow={() => {
            const blankRow: EncounterRow = {
              liklihood: Likihood.Normal,
              personality: '',
              relationship: '',
              creatures: ''
            }
            const rows = [...tableEdit.rows, blankRow]
            setTableEdit({...tableEdit, rows })

          }}
          onEditTable={(rowIdx: number, name: string, value: string | number) => {
            const modifiedRows = [...tableEdit.rows]
            const newRow = {...tableEdit.rows[rowIdx], [name]: value}
            modifiedRows[rowIdx] = newRow
            setTableEdit({...tableEdit, rows: modifiedRows})
          }}
          onDeleteTableRow={(rowIdx: number) => {
            const rows = tableEdit.rows.filter((_, idx) => idx !== rowIdx)
            setTableEdit({ ...tableEdit, rows })
          }}
          onRandomiseTableRow={(rowIdx, conf) => {
            const { numPcs, avgLvl, difficulty, environment } = conf
            const rows = [...tableEdit.rows]
            const newEncounterRow = generateEncounterRow(numPcs, avgLvl, difficulty, environment, rows[rowIdx].liklihood)
            rows[rowIdx] = newEncounterRow
            setTableEdit({ ...tableEdit, rows })
          }}
          onClose={() => { 
            createOrEditEncounterTable(tableEdit)
            setShowTable(null)
          }}
        />}
      <div className={styles.container}>
        <div className={styles.headerWrapper}>
          <div className={styles.header}>
            Encounter Generator
          </div>
          <Popup
            trigger={(<Icon size='large' onClick={() => setCalcPopupOpen(true)} className={styles.calculatorIcon} name='calculator' />)} 
            on='click'
            onClose={() => setCalcPopupOpen(false)}
            open={calcPopupOpen}
            basic
            position='bottom center'
          >
            <FuzzyCalc 
              className={styles.calculator} 
              getResult={fuzzyEncounterCalculate}
            />
          </Popup>
        </div>
        <div className={styles.buttons}>
          <ConfigPopup
            open={confPopupOpen}
            onClose={() => setConfPopupOpen(false)}
            defaultConfig={defaultEncounterConfig}
            onSubmit={(conf: EncounterConfig) => {
              saveLastEncounterConf(conf)
              const { numPcs, avgLvl, difficulty, environment } = conf
              setCurrEncounter(generateEncounter(numPcs, avgLvl, difficulty, environment))
              setConfPopupOpen(false)
            }}
            trigger={(
              <Button 
                onClick={() => setConfPopupOpen(true)} 
                className={styles.button} 
                content='Random Encounter' 
              />
            )} 
          />

          <Dropdown 
            className={styles.button}
            button
            value=''
            text='Random Encounter (table)'
          >
            <Dropdown.Menu>
              {tableOps.map((op, idx) => (
                <Dropdown.Item 
                  key={`table-${idx}`}
                  onClick={() => {
                    const table = tables.find(t => t.id === op.value)
                    if (!table) return
                    const encounter = selectFromTable(table)
                    setCurrEncounter(encounter)
                  }}
                >
                  <div className={styles.dropdownItem}>
                    {op.text}
                      <Dropdown 
                        icon='ellipsis horizontal'
                        className={styles.dropdownMenuButton}
                      >
                        <Dropdown.Menu>
                          <Dropdown.Item
                            onClick={() => {
                              const table = tables.find(t => t.id === op.value)
                              if (!table) return
                              setShowTable(op.value)
                              setTableEdit(table)
                            }}
                          >
                            <Icon name='pencil' />
                            edit
                          </Dropdown.Item>
                          <Dropdown.Item
                            onClick={() => deleteEncounterTable(op.value)} 
                          >
                            <Icon name='trash alternate' />
                            delete
                          </Dropdown.Item>
                        </Dropdown.Menu>
                      </Dropdown>
                    </div>
                </Dropdown.Item>
              ))}
              <Dropdown.Item 
                text='new table' 
                value='new' 
                icon='add'
                className={styles.newTable}
                onClick={() => { 
                  const newTable = generateNewTable()
                  if (!newTable) return
                  setShowTable(newTable.id)
                  setTableEdit(newTable)
                  createOrEditEncounterTable(newTable)
                }}
              />
            </Dropdown.Menu>
          </Dropdown>
        </div>
        <div className={styles.encounterInfo}>
          <div className={styles.backgroundImage} />
          <div className={classNames(styles.creatures, styles.infoBox)}>
            <h3 className={styles.infoHeader}>
              Creatures
            </h3>
            <div className={styles.text}>
              {currEncounter.creatures}
            </div>
          </div>
          <div className={classNames(styles.personality, styles.infoBox)}>
            <h3 className={styles.infoHeader}>
              Personality
            </h3>
            <div className={styles.text}>
              {currEncounter.personality}
            </div>
          </div>
          <div className={classNames(styles.relationships, styles.infoBox)}>
            <h3 className={styles.infoHeader}>
              Relationships
            </h3>
            <div className={styles.text}>
              {currEncounter.relationship}
            </div>
          </div>
        </div>
        {!loggedIn && 
        <div className={styles.pleaseLogin}>
          <Link to='/login'>Sign in</Link> to automatically save your encounter tables. 
        </div>}
      </div>
    </div>
  )
}

export default Encounters