import React from 'react'
import { connect } from 'react-redux'
import { DetermineInputs } from 'types/encounters'
import FuzzyCalc, { FuzzyCalcProps } from './fuzzy-calc'
import { Difficulty } from 'constants/enums'

export const FuzzyCalcContainer = (props: FuzzyCalcProps) => {
  return (
    <FuzzyCalc {...props} />
  )
}

export function mapStateToProps() {
  const storageConf = localStorage.getItem('calculatorConfig') || localStorage.getItem('encounterConfig')
  const defaultCalcConfig = storageConf ? JSON.parse(storageConf) : undefined

  const formDefaults: DetermineInputs = {
    numPcs: defaultCalcConfig?.numPcs || 4,
    avgLvl: defaultCalcConfig?.avgLvl || 1,
    difficulty: Difficulty.Medium
  }

  const saveCalcConf = (conf: Pick<DetermineInputs, 'numPcs' | 'avgLvl'>) => {
    localStorage.setItem('encounterConfig', JSON.stringify(conf))
  }
  
  return {
    formDefaults,
    saveCalcConf
  }
}

export default connect(mapStateToProps)(FuzzyCalcContainer)