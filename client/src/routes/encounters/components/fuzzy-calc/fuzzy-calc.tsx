import React, { useState } from 'react'
import classNames from 'classnames'
import styles from './fuzzy-calc.module.scss'
import { FuzzyDetermine, Difficulty } from 'constants/enums'
import { DetermineInputs } from 'types/encounters'
import { ValueOf } from 'types/common'
import RadioButtonList from 'components/RadioButtonList'
import { Option } from 'types/common'
import { Button, Dropdown, Input, Segment } from 'semantic-ui-react'
import MonsterCrs from './monster-crs'
import { BLANK_CR } from 'utils/encounter-generator/constants'
import { getDifficultyLabel } from 'utils/encounter-generator/difficulty'

const getCrLabel = (cr: number): string => {
  switch (cr) {
    case -1: 
      return '~'
    case 0.125:
      return '1/8'
    case 0.25:
      return '1/4'
    case 0.5:
      return '1/2'
    default: 
      return `${cr}`
  }
}

const inputMap: { [det in FuzzyDetermine]: { name: string, inputs: (keyof DetermineInputs)[] } } = {
  [FuzzyDetermine.NumPlayers]: { 
    name: 'Number of players',
    inputs: [
      'avgLvl',
      'monsterCrs',
      'difficulty'
    ]
  },
  [FuzzyDetermine.AvgLvl]: { 
    name: 'Average player level',
    inputs: [
      'numPcs',
      'monsterCrs',
      'difficulty'
    ]
  },
  [FuzzyDetermine.NumMonsters]: { 
    name: 'Number of monsters',
    inputs: [
      'numPcs',
      'monsterCr',
      'avgLvl',
      'difficulty'
    ]
  },
  [FuzzyDetermine.Difficulty]: { 
    name: 'Encounter difficulty',
    inputs: [
      'monsterCrs',
      'numPcs',
      'avgLvl'
    ]
  },
  [FuzzyDetermine.MonsterCr]: { 
    name: `Monster CR's`,
    inputs: [
      'numPcs',
      'numMonsters',
      'avgLvl',
      'difficulty'
    ]
  },
}

const radioOps: Option<number>[] = [
  {
    label: `Monster CR's`,
    value: FuzzyDetermine.MonsterCr
  },
  {
    label: 'Number of players',
    value: FuzzyDetermine.NumPlayers
  },
  {
    label: 'Average player level',
    value: FuzzyDetermine.AvgLvl
  },
  {
    label: 'Number of monsters (same CR)',
    value: FuzzyDetermine.NumMonsters
  },
  {
    label: 'Encounter difficulty',
    value: FuzzyDetermine.Difficulty
  },
]

const renderResult = (result: number | number[], type: FuzzyDetermine) => {
  let text
  let prettyResult = `${result}`
  switch(type) {
    case FuzzyDetermine.AvgLvl:
      text = `Average player level should be: `
      break
    case FuzzyDetermine.Difficulty:
      text = `Difficulty is: `
      prettyResult = getDifficultyLabel(result as number)
      break
    case FuzzyDetermine.MonsterCr:
      text = `Monster CR's should be: `
      prettyResult = (result as number[]).map(cr => `CR ${cr}`).join(', ')
      break
    case FuzzyDetermine.NumPlayers:
      text = `Number of players to balance: `
      break
    case FuzzyDetermine.NumMonsters:
      text = `Number of monsters should be: `
      break
  }


  return (
    <div className={styles.resultWrap}>
      <div className={styles.resultText}>
        {text}
      </div>
      <div className={styles.resultValue}>
        {prettyResult}
      </div>
    </div>
  )

}

export interface FuzzyCalcProps {
  className?: string
  formDefaults?: DetermineInputs
  saveCalcConf?: (conf: Pick<DetermineInputs, 'avgLvl' | 'numPcs'>) => void
  getResult: (determine: FuzzyDetermine, inputs: DetermineInputs) => number | number[] | null
}

const FuzzyCalc = (props: FuzzyCalcProps) => {
  const { className, formDefaults = {}, getResult, saveCalcConf } = props

  const [gatheredInput, setGatheredInput] = useState<DetermineInputs>(formDefaults)
  const [determining, setDetermining] = useState<FuzzyDetermine>(FuzzyDetermine.MonsterCr)
  const [formIdx, setFormIdx] = useState<number | null>(null)
  const [result, setResult] = useState<number | number[] | null>(null)

  const difficultyOps = []
  for (const [text, value] of Object.entries(Difficulty)) {
    if (isNaN(parseInt(text))) {
      difficultyOps.push({text: text.toLowerCase(), value})
    }
  }

  const inputComponents = {
    'avgLvl': {
      desc: 'What is the average level of your player characters?',
      form: (
        <div >
          Level 
          <Dropdown 
            inline
            scrolling
            key='avgLvl'
            name='avgLvl'
            className={styles.inlineDropdown} 
            options={[...Array(20).keys()].map(lvl => ({ text: `${lvl + 1}`, value: lvl + 1 }))}
            onChange={(_, { value }) => setGatheredInput({ ...gatheredInput, avgLvl: value as number})}
            value={gatheredInput.avgLvl}
          />
        </div>
      ),
    },
    'monsterCr': {
      desc: 'What will the CR of the monsters be (This will be applied to all monsters)?',
      form: (
        <div onClick={() => console.log(gatheredInput)}>
          CR 
          <Dropdown 
            inline
            scrolling
            key='monsterCr'
            name='monsterCr'
            className={styles.inlineDropdown} 
            options={[0.125, 0.25, 0.5, ...Array(25).keys()]
              .filter(lvl => lvl !== 0)
              .map(lvl => ({ text: getCrLabel(lvl), value: lvl}))
            }
            onChange={(_, { value }) => setGatheredInput({ ...gatheredInput, monsterCr: value as number})}
            value={gatheredInput.monsterCr}
          />
        </div>
      )
    },
    'monsterCrs': {
      desc: 'What is CR of each monster in your encounter?',
      form: (
        <MonsterCrs 
          monsterCrs={gatheredInput.monsterCrs} 
          getCrLabel={getCrLabel}
          onAddQuant={(cr: number) => { 
            if (cr === BLANK_CR) return
            setGatheredInput({...gatheredInput, monsterCrs: [...(gatheredInput.monsterCrs || []), cr]})
          }}
          onSubtractQuant={(crToRemove: number) => {
            if (!gatheredInput.monsterCrs || crToRemove === BLANK_CR) return
            const newCrs = []
            // Remove  first instance of this CR
            let removed = false
            for (const cr of gatheredInput.monsterCrs) {
              if (cr === crToRemove && !removed) {
                removed = true
                continue
              }
              newCrs.push(cr)
            }
            setGatheredInput({ ...gatheredInput, monsterCrs: newCrs })
          }}
          onChangeCr={(oldCr: number, newCr: number) => {
            if (!gatheredInput.monsterCrs) return
            const newCrs = gatheredInput.monsterCrs.map(cr => cr === oldCr ? newCr : cr)
            setGatheredInput({ ...gatheredInput, monsterCrs: newCrs })
          }}
          onAddCr={() => { 
            if (gatheredInput.monsterCrs && gatheredInput.monsterCrs.includes(BLANK_CR)) return
            setGatheredInput({...gatheredInput, monsterCrs: [...(gatheredInput.monsterCrs || []), BLANK_CR]})
          }}
        />
      )
    },
    'difficulty': {
      desc: 'What is the difficulty of your encounter?',
      form: (
        <div >
          Difficulty 
          <Dropdown 
            inline
            scrolling
            className={styles.inlineDropdown} 
            options={difficultyOps}
            onChange={(_, { value }) => setGatheredInput({ ...gatheredInput, difficulty: value as Difficulty})}
            value={gatheredInput.difficulty}
          />
        </div>
      )
    },
    'numMonsters': {
      desc: 'How many monsters will be in your encounter?',
      form: (
        <div>
          Monster Count
          <Input 
            value={gatheredInput.numMonsters}
            type='number'
            onChange={(_, { value }) => setGatheredInput({ ...gatheredInput, numMonsters: value as unknown as number})}
          />
        </div>
      )
    },
    'numPcs': {
      desc: 'How many player characters are in your encounter?',
      form: (
        <div >
          Player Count 
          <Dropdown 
            inline
            scrolling
            className={styles.inlineDropdown} 
            options={[...Array(8).keys()].map(lvl => ({ text: `${lvl + 1}`, value: lvl + 1}))
            }
            onChange={(_, { value }) => setGatheredInput({ ...gatheredInput, numPcs: value as number})}
            value={gatheredInput.numPcs}
          />
        </div>
      )
    },
  }
  
  const determineMeta = inputMap[determining]
  const currentComponent = formIdx !== null 
    ? inputComponents[determineMeta.inputs[formIdx]] 
    : undefined
  
  const isLastForm = formIdx === determineMeta.inputs.length - 1

  return (
    <div className={classNames(styles.root, className)}>
      {(!currentComponent && result === null) &&
        <div>
          <div className={styles.promptParagraph}>
            What's the unknown variable in your encounter that you'd like to determine?
          </div>
          <RadioButtonList
            onChange={(newVal: FuzzyDetermine) => setDetermining(newVal)}
            selected={determining}
            options={radioOps}
          />
        </div>
      }
      {(!!currentComponent && result === null) &&
        <div className={styles.content}>
          <div className={styles.promptParagraph}>
            {currentComponent.desc}
          </div>
          {currentComponent.form}
        </div>
      }
      {result !== null &&
        <div className={styles.result}>
          {renderResult(result, determining)}
        </div>
      }
      <div className={styles.buttons}>
        {formIdx !== null &&
          <Button 
            className={styles.back}
            onClick={() => { 
              if (formIdx === 0) setFormIdx(null)
              else setFormIdx(formIdx - 1)
            }}
          >
            Back
          </Button>
        }
        <Button 
          className={styles.next}
          onClick={() => { 
            if (isLastForm) {
              setResult(getResult(determining, gatheredInput))
              const newConf = {
                avgLvl: gatheredInput.avgLvl || formDefaults.avgLvl,
                numPcs: gatheredInput.numPcs || formDefaults.numPcs
              }
              setFormIdx(null)
              saveCalcConf && saveCalcConf(newConf)
            } else {
              if (formIdx === null) { 
                if (result !== null) {
                  setResult(null)
                  setFormIdx(null)
                  setGatheredInput(formDefaults)
                } else {
                  setFormIdx(0)
                }
              }
              else setFormIdx(formIdx + 1)
            }
          }}
        >
          {(isLastForm && result === null) && 'Determine'}
          {(!isLastForm && result === null) && 'Next'}
          {result !== null && 'Finish'}
        </Button>
      </div>
    </div>
  )
}

export default FuzzyCalc