import React from 'react'
import styles from './monster-crs.module.scss'
import classNames from 'classnames'
import { Segment, Dropdown, Button } from 'semantic-ui-react'
import { BLANK_CR } from 'utils/encounter-generator/constants'

interface MonsterCrsProps {
  monsterCrs?: number[]
  onAddCr: () => void
  getCrLabel: (cr: number) => string
  onChangeCr: (oldCr: number, newCr: number) => void
  onSubtractQuant: (cr: number) => void
  onAddQuant: (cr: number) => void
}

const MonsterCrs = (props: MonsterCrsProps) => {
  const { monsterCrs = [], onAddCr, onAddQuant, onSubtractQuant, onChangeCr, getCrLabel } = props
  
  const crReducer: {[cr: number]: number} = {}
  for (const cr of monsterCrs) {
    if(!crReducer[cr]) crReducer[cr] = 0
    crReducer[cr]++
  }

  const groupedCrs = []
  for (const [cr, qty] of Object.entries(crReducer)) {
    const parsedCr = parseFloat(cr)
    if (parsedCr === BLANK_CR) continue
    groupedCrs.push({ cr: parsedCr, qty })
  }

  if (crReducer[BLANK_CR]) {
    for (let i = 0; i < crReducer[BLANK_CR]; i++) {
      groupedCrs.push({ cr: -1, qty: 0 })
    }
  }

  groupedCrs.sort((a,b) => a.cr - b.cr).reverse()

  const crOps = [0.125, 0.25, 0.5, ...Array(26).keys()]
    .filter(lvl => lvl !== 0)
    .map(lvl => ({ text: getCrLabel(lvl), value: lvl}))

  return (
    <div className={styles.root}>
      {groupedCrs.map((groupedCr, idx) => (
          <Segment key={`cr-${idx}`} className={styles.crSegment}>
            <div>
              CR
              <Dropdown 
                inline
                scrolling
                className={styles.inlineDropdown} 
                options={crOps}
                onChange={(_, { value }) => onChangeCr(groupedCr.cr, value as number)}
                value={groupedCr.cr}
              />
            </div>
            <div>
              <Button 
                size='mini'
                className={classNames(styles.qtyButton, styles.minus)}
                onClick={() => onSubtractQuant(groupedCr.cr)}
                icon='minus' 
                circular
                active={false}
              />
              {groupedCr.qty}
              <Button 
                active={false}
                size='mini'
                onClick={() => onAddQuant(groupedCr.cr)}
                className={classNames(styles.qtyButton, styles.add)}
                icon='add' 
                circular
              />
            </div>
          </Segment>
        )) 
      }
      <div onClick={onAddCr} className={styles.addCr}>
        add +
      </div>
    </div>
  ) 
}

export default MonsterCrs