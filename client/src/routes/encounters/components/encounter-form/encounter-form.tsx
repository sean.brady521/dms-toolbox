import React from 'react'
import styles from './encounter-form.module.scss'
import { EncounterTableBase, EncounterRow, EncounterConfig } from 'types/encounters'
import { Modal, Dropdown, TextArea, Input } from 'semantic-ui-react'
import DataTable from 'components/data-table'
import { DataCell } from 'components/data-table/data-table-types'
import { Difficulty } from 'constants/enums'
import { personalities, relationships, probabilities } from 'utils/encounter-generator/constants'

const dummyData: EncounterRow[] = [
  {
    creatures: '5 owls',
    personality: 'cowardly',
    relationship: 'rival',
    liklihood: 2
  },
  {
    creatures: '4 spys, 1 battle mage',
    personality: 'braggart',
    relationship: 'outcast',
    liklihood: 4
  },
  {
    creatures: '2 goblins',
    personality: 'fanatic',
    relationship: 'bully',
    liklihood: 3
  },
  {
    creatures: '3 owls, 1 goblin, 2 bugbears',
    personality: 'joker',
    relationship: 'mercenary',
    liklihood: 2
  },
  {
    creatures: '5 owls, 2 faerie dragons',
    personality: 'brave',
    relationship: 'abused',
    liklihood: 4
  },
  {
    creatures: '2 orcs, 3 goblins',
    personality: 'rabble',
    relationship: 'worshipped',
    liklihood: 1
  },
]



const dummyTable: EncounterTableBase = {
  id: '',
  name: 'cool table',
  rows: dummyData
}

type tableVal = string | number

interface EncounterFormProps {
  table?: EncounterTableBase
  open: boolean
  defaultEncounterConfig: EncounterConfig
  onClose: () => void
  onAddTableRow: (difficulty?: Difficulty) => void
  onEditTable: (rowIdx: number, name: string, value: tableVal) => void
  onChangeName: (newName: string) => void
  onDeleteTableRow: (rowIdx: number) => void
  onRandomiseTableRow: (rowIdx: number, conf: EncounterConfig) => void
}


const EncounterForm = (props: EncounterFormProps) => {
  const { 
    table = dummyTable, 
    open,
    defaultEncounterConfig,
    onClose,
    onAddTableRow,
    onEditTable,
    onChangeName,
    onDeleteTableRow,
    onRandomiseTableRow,
  } = props

  const personalityOps = personalities.map(p => ({text: p.type, value: p.type.toLowerCase()}))
  const relationshipOps = relationships.map(r => ({text: r.type, value: r.type.toLowerCase()}))


  const rows: DataCell[][] = []
  for (let i = 0; i < table.rows.length; i++) {
    const rowMeta = table.rows[i]
    const relationshipType = relationships.find(r => r.type.toLowerCase() === rowMeta.relationship)?.type || ''
    const personalityType = personalities.find(r => r.type.toLowerCase() === rowMeta.personality)?.type || ''
    const liklihoodVal = probabilities.find(r => r.value === rowMeta.liklihood)?.value || 2 

    rows.push([
      <TextArea 
        className={styles.gridTextarea}
        onChange={(_, { value }) => onEditTable(i, 'creatures', value as tableVal)} 
        rows={1}
        value={rowMeta.creatures || ''} 
      />,
      <Dropdown 
        className={styles.gridDropdown}
        value={relationshipType.toLowerCase()} 
        placeholder='No Selection'
        options={relationshipOps} 
        onChange={(_, { value }) => onEditTable(i, 'relationship', value as tableVal)} 
      />,
      <Dropdown 
        className={styles.gridDropdown}
        value={personalityType.toLowerCase()} 
        placeholder='No Selection'
        options={personalityOps} 
        onChange={(_, { value }) => onEditTable(i, 'personality', value as tableVal)} 
      />,
      <Dropdown 
        className={styles.gridDropdown}
        value={liklihoodVal} 
        options={probabilities} 
        placeholder='No Selection'
        onChange={(_, { value }) => onEditTable(i, 'liklihood', value as tableVal)} 
      />,
    ])
  }

  return (
    <Modal 
      open={open}
      size='large'
      className={styles.modal}
      onClose={onClose}
    >
      <div className={styles.root}>
        <Input 
          value={table.name} 
          onChange={(_, { value }) => onChangeName(value)}
          className={styles.input}
          transparent
          size='huge'
        />
        <DataTable 
          className={styles.table}
          defaultEncounterConfig={defaultEncounterConfig}
          onNewRow={onAddTableRow}
          onDeleteRow={onDeleteTableRow}
          onRandomiseRow={onRandomiseTableRow}
          headers={['monsters', 'personality', 'relationships', 'liklihood']} 
          rows={rows}
        />
      </div>
    </Modal>
  )
}

export default EncounterForm