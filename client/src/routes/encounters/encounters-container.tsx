import React, { useEffect } from 'react'
import Encounters, { EncountersProps } from './encounters'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import { fetchEncounterTables, postEncounterTable, removeEncounterTable } from 'store/encounters/encounters-actions'
import { EncounterTable, EncounterTableBase, EncounterConfig } from 'types/encounters'
import { getUserId } from 'utils/auth'
import parseLocation from 'utils/parse-location'

const { v4: uuidv4 } = require('uuid')

interface EncountersContainerProps extends EncountersProps {
  fetchEncounterTables: () => void
}

export const EncountersContainer = ({ fetchEncounterTables, ...props }: EncountersContainerProps) => {
  useEffect(() => {
    fetchEncounterTables()
    window.scrollTo(0, 0)
  }, [])

  return (
    <Encounters {...props} />
  )
}

export function mapStateToProps(state: StoreState) {
  const { loggedIn } = state.user
  const { tables, loading, error } = state.encounters

  const generateNewTable = (): EncounterTable | null => {
    const uid = getUserId()

    return {
      id: uuidv4(),
      modifiedAt: Date.now(),
      createdAt: Date.now(),
      createdBy: uid ? uid : 'guest',
      name: 'Unnamed Table',
      rows: [],
    }
  }

  const storageConf = localStorage.getItem('encounterConfig')
  const defaultEncounterConfig = storageConf ? JSON.parse(storageConf) : undefined

  const saveLastEncounterConf = (conf: EncounterConfig) => {
    localStorage.setItem('encounterConfig', JSON.stringify(conf))
  }

  const { fuzzyCalc } = parseLocation(state.router.location)
  
  return {
    tables,
    defaultEncounterConfig,
    defaultCalcOpen: fuzzyCalc === 'open',
    loggedIn,
    loading,
    error,
    saveLastEncounterConf,
    generateNewTable
  }
}

export function mapDispatchToProps(dispatch: any) {
  return {
    fetchEncounterTables: () => dispatch(fetchEncounterTables()),
    createOrEditEncounterTable: (table: Partial<EncounterTable>) => dispatch(postEncounterTable({ ...table, modifiedAt: Date.now() })),
    deleteEncounterTable: (id: string) => dispatch(removeEncounterTable(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EncountersContainer)