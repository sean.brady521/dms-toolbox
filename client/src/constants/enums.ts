export enum Likihood {
  VeryUnlikely = 1,
  Unlikely = 2,
  Normal = 3,
  Likely = 4,
  VeryLikely = 6,
}

export enum Difficulty {
  Easy = 1,
  Medium = 2,
  Difficult = 3,
  Deadly = 4
}

export enum Environment {
  Nature = 'nature',
  Dungeon = 'dungeon',
  OtherPlane = 'other plane',
  Underground = 'underground',
  City = 'city',
  Forest = 'forest',
  Water = 'water' 
}

export enum FuzzyDetermine {
  NumPlayers = 1,
  AvgLvl = 2,
  NumMonsters = 3,
  Difficulty = 4,
  MonsterCr = 5
}