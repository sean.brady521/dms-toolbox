export const firebaseConf = {
  apiKey: "AIzaSyD7X_9yxQqccKDYhVZqciEEyYrY1YpuwTY",
  authDomain: "dms-toolbox.firebaseapp.com",
  databaseURL: "https://dms-toolbox.firebaseio.com",
  projectId: "dms-toolbox",
  storageBucket: "dms-toolbox.appspot.com",
  messagingSenderId: "981092419531",
  appId: "1:981092419531:web:1ffe985c16b190fbe2584e",
}

export const actionCodeSettings = {
  url: process.env.NODE_ENV === 'development' 
    ? 'http://localhost:3000' 
    : 'https://dungeonmasters.tools',
  handleCodeInApp: true,
}