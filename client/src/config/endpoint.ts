import endpoints from './endpoints.json'

function getEndpointBase () {
  const env = process.env.NODE_ENV
  return env === 'development' ? endpoints.devBaseUrl : endpoints.prodBaseUrl
}

export default getEndpointBase
