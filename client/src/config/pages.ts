export interface Page {
  value: string 
  label: string
  requireAuth?: boolean
}

const pages = [
  {
    value: 'home',
    label: 'Home'
  },
  {
    value: 'npcs',
    label: 'Npcs',
    requireAuth: false
  },
  {
    value: 'campaigns',
    label: 'Campaigns',
    requireAuth: false
  },
  {
    value: 'speed-roller',
    label: 'Speed Roller',
    requireAuth: false
  },
  {
    value: 'encounters',
    label: 'Encounters',
    requireAuth: false
  }
]
export default pages

