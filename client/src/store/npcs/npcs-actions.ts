import { INpc } from '../../types/npcs'
import { fetchAll, addOrModifyEntry, removeById } from 'utils/server'
import { Dispatch } from 'redux'
import { SortConfig, StoreState } from 'types/common'

const _removeNpcBegin = () => ({
  type: 'REMOVE_NPC_BEGIN',
})

const _removeNpcSuccess = (id: string) => ({
  type: 'REMOVE_NPC_SUCCESS',
  payload: { id }
})

const _removeNpcFailure = (error: Error) => ({
  type: 'REMOVE_NPC_FAILURE',
  payload: { error }
})

const _postNpcBegin = () => ({
  type: 'POST_NPC_BEGIN',
})

const _postNpcSuccess = (npc: Partial<INpc>) => ({
  type: 'POST_NPC_SUCCESS',
  payload: { npc }
})

const _postNpcFailure = (error: Error) => ({
  type: 'POST_NPC_FAILURE',
  payload: { error }
})

const _fetchNpcsBegin = () => ({
  type: 'FETCH_NPCS_BEGIN',
})

const _fetchNpcsSuccess = (npcs: INpc[]) => ({
  type: 'FETCH_NPCS_SUCCESS',
  payload: { npcs }
})

const _fetchNpcsFailure = (error: Error) => ({
  type: 'FETCH_NPCS_FAILURE',
  payload: { error }
})

export const setSearch = (search: string) => ({
  type: 'SET_SEARCH',
  payload: { search }
})

export const setSort = (sortConfig: SortConfig) => ({
  type: 'SET_SORT',
  payload: { sortConfig }
})

export const setSelectedCampaign = (selectedCampaign: string) => ({
  type: 'SET_CAMPAIGN',
  payload: { selectedCampaign }
})

export const removeNpc = (id: string) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { npcs, user } = getState()

    if (!user.loggedIn) {
      dispatch(_removeNpcSuccess(id))
      return
    }

    const onSuccess = () => {
      dispatch(_removeNpcSuccess(id))
      window.localStorage.setItem('npc_count', `${npcs.npcs.length}`)
    }
    const onFailure = (err: any) => dispatch(_removeNpcFailure(err))
    dispatch(_removeNpcBegin())
    return removeById('npc', id, onSuccess, onFailure)
  }
}

export const postNpc = (npc: Partial<INpc>) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { npcs, user } = getState()

    if (!user.loggedIn) { 
      dispatch(_postNpcSuccess(npc))
      return
    }

    const onSuccess = () => { 
      dispatch(_postNpcSuccess(npc))
      window.localStorage.setItem('npc_count', `${npcs.npcs.length}`)
    }
    const onFailure = (err: any) => dispatch(_postNpcFailure(err))
    dispatch(_postNpcBegin())
    return addOrModifyEntry('npc', npc, onSuccess, onFailure)
  }
}

export const fetchNpcs = () => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { npcs, user } = getState()

    if (!user.loggedIn) return

    if (npcs.npcs.length ||npcs.loading) return // Weve already fetched

    const onSuccess = (npcs: INpc[]) => { 
      dispatch(_fetchNpcsSuccess(npcs))
      window.localStorage.setItem('npc_count', `${npcs.length}`)
    }
    const onFailure = (npcs: any) => dispatch(_fetchNpcsFailure(npcs))
    dispatch(_fetchNpcsBegin())
    return fetchAll('npcs', onSuccess, onFailure)
  }
}
