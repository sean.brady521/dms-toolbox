import { INpc } from 'types/npcs';
import { SortConfig } from 'types/common';

export const REMOVE_NPC_BEGIN = 'REMOVE_NPC_BEGIN'
export const REMOVE_NPC_SUCCESS = 'REMOVE_NPC_SUCCESS'
export const REMOVE_NPC_FAILURE = 'REMOVE_NPC_FAILURE'
export const POST_NPC_BEGIN = 'POST_NPC_BEGIN'
export const POST_NPC_SUCCESS = 'POST_NPC_SUCCESS'
export const POST_NPC_FAILURE = 'POST_NPC_FAILURE'
export const FETCH_NPCS_BEGIN = 'FETCH_NPCS_BEGIN'
export const FETCH_NPCS_SUCCESS = 'FETCH_NPCS_SUCCESS'
export const FETCH_NPCS_FAILURE = 'FETCH_NPCS_FAILURE'
export const SET_SEARCH = 'SET_SEARCH'
export const SET_SORT = 'SET_SORT'
export const SET_CAMPAIGN = 'SET_CAMPAIGN'

interface ActionFail {
  type: typeof REMOVE_NPC_FAILURE | typeof POST_NPC_FAILURE | typeof FETCH_NPCS_FAILURE
  payload: {
    error: string
  }
}

interface ActionBegin {
  type: typeof REMOVE_NPC_BEGIN | typeof POST_NPC_BEGIN | typeof FETCH_NPCS_BEGIN
}

interface PostNpcAction {
  type: typeof POST_NPC_SUCCESS
  payload: {
    npc: INpc
  }
}

interface RemoveNpcAction {
  type: typeof REMOVE_NPC_SUCCESS
  payload: {
    id: string
  }
}

interface GetNpcsAction {
  type: typeof FETCH_NPCS_SUCCESS
  payload: {
    npcs: INpc[]
  }
}

interface SetSearch {
  type: typeof SET_SEARCH
  payload: {
    search: string
  }
}

interface SetCampaign {
  type: typeof SET_CAMPAIGN
  payload: {
    selectedCampaign: string
  }
}

interface SetSort {
  type: typeof SET_SORT
  payload: {
    sortConfig: SortConfig
  }
}



export type NpcActionTypes = ActionFail | ActionBegin | PostNpcAction | RemoveNpcAction | GetNpcsAction | SetSearch | SetSort | SetCampaign