import { NpcsState } from 'types/common'
import { INpc } from 'types/npcs'
import {
  REMOVE_NPC_BEGIN,
  REMOVE_NPC_SUCCESS,
  REMOVE_NPC_FAILURE,
  POST_NPC_BEGIN,
  POST_NPC_SUCCESS,
  POST_NPC_FAILURE,
  FETCH_NPCS_BEGIN,
  FETCH_NPCS_SUCCESS,
  FETCH_NPCS_FAILURE,
  SET_SEARCH,
  SET_SORT,
  SET_CAMPAIGN,
  NpcActionTypes
} from './npcs-action-types'

const initialState: NpcsState = {
  npcs: [],
  loading: false,
  error: null,
  selectedCampaign: window.localStorage.getItem('selectedCampaign') || 'all',
  search: '',
  sortConfig: { direction: 'asc', field: 'createdAt' }
}

function updateOrAdd(currNpcs: INpc[], incNpc: INpc) {
  const newNpcs = [...currNpcs]

  let found = false
  for (let i = 0 ; i < newNpcs.length ; i++) {
    if (newNpcs[i].id === incNpc.id) {
      newNpcs[i] = incNpc
      found = true
      break
    }
  }
  if (!found) newNpcs.push(incNpc)
  return newNpcs
}

export default (
  state = initialState,
  action: NpcActionTypes
  ): NpcsState => {
  switch (action.type) {
    case REMOVE_NPC_BEGIN:
      return {
        ...state,
        error: null
      }
    case REMOVE_NPC_SUCCESS:
      return {
        ...state,
        npcs: state.npcs.filter(npc => npc.id !== action.payload.id)
      }
    case REMOVE_NPC_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      }
    case POST_NPC_BEGIN:
      return {
        ...state,
        error: null
      }
    case POST_NPC_SUCCESS:
      return {
        ...state,
        npcs: updateOrAdd(state.npcs, action.payload.npc)
      }
    case POST_NPC_FAILURE:
      return {
        ...state,
        error: action.payload.error,
      }
    case FETCH_NPCS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_NPCS_SUCCESS:
      return {
        ...state,
        loading: false,
        npcs: action.payload.npcs
      }
    case FETCH_NPCS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        npcs: []
      }
    case SET_SEARCH:
      return {
        ...state,
        search: action.payload.search
      }
    case SET_SORT:
      return {
        ...state,
        sortConfig: action.payload.sortConfig
      }
    case SET_CAMPAIGN:
      return {
        ...state,
        selectedCampaign: action.payload.selectedCampaign
      }
    default:
      return state
  }
}