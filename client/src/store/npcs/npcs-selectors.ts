import { INpc } from "types/npcs";
import { SortConfig } from "types/common";
import { alphaSort } from "utils/sort";

export function selectFilteredNpcs (npcs: INpc[], search: string, campaign: string): INpc[] {
  if (!search && campaign === 'all') return npcs
  const filteredNpcs = []
  const lSearch = search.toLowerCase()
  for (const npc of npcs) {
    let passed = true
    const searchText = `${npc.name || ''}${npc.context || ''}`.toLowerCase()
    if (searchText && !searchText.includes(lSearch)) {
      passed = false
    }
    if (npc.campaign !== campaign && campaign !== 'all') {
      passed = false
    }
    if (passed) {
      filteredNpcs.push(npc)
    }
  }
  return filteredNpcs
}

export function selectSortedNpcs (npcs: INpc[], sortConf: SortConfig): INpc[] {
  if (!npcs.length) return []
  const { field, direction } = sortConf
  // @ts-ignore
  const example = npcs[0][field]
  let ascSorted
  if (isNaN(example)) {
    ascSorted = npcs.sort(alphaSort(field))
  } else {
    // @ts-ignore
    ascSorted = npcs.sort((a,b) => parseInt(a[field]) - parseInt(b[field]))
  }
  return direction === 'asc' ? ascSorted : ascSorted.reverse()
}