import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'

import userReducer from './user/user-reducers'
import npcsReducer from './npcs/npcs-reducers'
import campaignReducers from './campaigns/campaigns-reducers'
import encountersReducers from './encounters/encounters-reducers'

export const history = createBrowserHistory()

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const initialState = {}

export default () => {
  const store = createStore(
    combineReducers({
      router: connectRouter(history),
      user: userReducer,
      npcs: npcsReducer,
      campaigns: campaignReducers,
      encounters: encountersReducers
    }),
    initialState,
    composeEnhancers(
      applyMiddleware(thunk, routerMiddleware(history))
    )
  )

  return store
}
