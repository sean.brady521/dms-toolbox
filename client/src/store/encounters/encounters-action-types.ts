import { EncounterTable } from 'types/encounters';

export const REMOVE_ENCOUNTER_TABLE_BEGIN = 'REMOVE_ENCOUNTER_TABLE_BEGIN'
export const REMOVE_ENCOUNTER_TABLE_SUCCESS = 'REMOVE_ENCOUNTER_TABLE_SUCCESS'
export const REMOVE_ENCOUNTER_TABLE_FAILURE = 'REMOVE_ENCOUNTER_TABLE_FAILURE'
export const POST_ENCOUNTER_TABLE_BEGIN = 'POST_ENCOUNTER_TABLE_BEGIN'
export const POST_ENCOUNTER_TABLE_SUCCESS = 'POST_ENCOUNTER_TABLE_SUCCESS'
export const POST_ENCOUNTER_TABLE_FAILURE = 'POST_ENCOUNTER_TABLE_FAILURE'
export const FETCH_ENCOUNTER_TABLES_BEGIN = 'FETCH_ENCOUNTER_TABLES_BEGIN'
export const FETCH_ENCOUNTER_TABLES_SUCCESS = 'FETCH_ENCOUNTER_TABLES_SUCCESS'
export const FETCH_ENCOUNTER_TABLES_FAILURE = 'FETCH_ENCOUNTER_TABLES_FAILURE'

interface ActionFail {
  type: typeof REMOVE_ENCOUNTER_TABLE_FAILURE | typeof POST_ENCOUNTER_TABLE_FAILURE | typeof FETCH_ENCOUNTER_TABLES_FAILURE
  payload: {
    error: string
  }
}

interface ActionBegin {
  type: typeof REMOVE_ENCOUNTER_TABLE_BEGIN | typeof POST_ENCOUNTER_TABLE_BEGIN | typeof FETCH_ENCOUNTER_TABLES_BEGIN
}

interface PostEncounterTableAction {
  type: typeof POST_ENCOUNTER_TABLE_SUCCESS
  payload: {
    table: EncounterTable
  }
}

interface RemoveEncounterTableAction {
  type: typeof REMOVE_ENCOUNTER_TABLE_SUCCESS
  payload: {
    id: string
  }
}

interface GetEncounterTablesAction {
  type: typeof FETCH_ENCOUNTER_TABLES_SUCCESS
  payload: {
    tables: EncounterTable[]
  }
}


export type EncounterActionTypes = ActionFail | ActionBegin | PostEncounterTableAction | RemoveEncounterTableAction | GetEncounterTablesAction