import { EncounterTable } from '../../types/encounters'
import { fetchAll, addOrModifyEntry, removeById } from 'utils/server'
import { Dispatch } from 'redux'
import { StoreState } from 'types/common'

const _removeEncounterTableBegin = () => ({
  type: 'REMOVE_ENCOUNTER_TABLE_BEGIN',
})

const _removeEncounterTableSuccess = (id: string) => ({
  type: 'REMOVE_ENCOUNTER_TABLE_SUCCESS',
  payload: { id }
})

const _removeEncounterTableFailure = (error: Error) => ({
  type: 'REMOVE_ENCOUNTER_TABLE_FAILURE',
  payload: { error }
})

const _postEncounterTableBegin = () => ({
  type: 'POST_ENCOUNTER_TABLE_BEGIN',
})

const _postEncounterTableSuccess = (table: Partial<EncounterTable>) => ({
  type: 'POST_ENCOUNTER_TABLE_SUCCESS',
  payload: { table }
})

const _postEncounterTableFailure = (error: Error) => ({
  type: 'POST_ENCOUNTER_TABLE_FAILURE',
  payload: { error }
})

const _fetchEncounterTablesBegin = () => ({
  type: 'FETCH_ENCOUNTER_TABLES_BEGIN',
})

const _fetchEncounterTablesSuccess = (tables: EncounterTable[]) => ({
  type: 'FETCH_ENCOUNTER_TABLES_SUCCESS',
  payload: { tables }
})

const _fetchEncounterTablesFailure = (error: Error) => ({
  type: 'FETCH_ENCOUNTER_TABLES_FAILURE',
  payload: { error }
})

export const removeEncounterTable = (id: string) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    if (!getState().user.loggedIn) {
      dispatch(_removeEncounterTableSuccess(id))
      return
    }

    const onSuccess = () => dispatch(_removeEncounterTableSuccess(id))
    const onFailure = (err: any) => dispatch(_removeEncounterTableFailure(err))
    dispatch(_removeEncounterTableBegin())
    return removeById('encounter-table', id, onSuccess, onFailure)
  }
}

export const postEncounterTable = (encounter: Partial<EncounterTable>) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    if (!getState().user.loggedIn) {
      dispatch(_postEncounterTableSuccess(encounter))
      return
    }
    const onSuccess = () => dispatch(_postEncounterTableSuccess(encounter))
    const onFailure = (err: any) => dispatch(_postEncounterTableFailure(err))
    dispatch(_postEncounterTableBegin())
    return addOrModifyEntry('encounter-table', encounter, onSuccess, onFailure)
  }
}

export const fetchEncounterTables = () => {
  return (dispatch: Dispatch, getState: () => StoreState) => {

    const { encounters, user } = getState()

    if (encounters.tables.length || !user.loggedIn) return

    const onSuccess = (tables: any) => dispatch(_fetchEncounterTablesSuccess(tables))
    const onFailure = (tables: any) => dispatch(_fetchEncounterTablesFailure(tables))
    dispatch(_fetchEncounterTablesBegin())
    return fetchAll('encounter-tables', onSuccess, onFailure)
  }
}
