import { EncountersState } from 'types/common'
import { EncounterTable } from 'types/encounters'
import {
  REMOVE_ENCOUNTER_TABLE_BEGIN,
  REMOVE_ENCOUNTER_TABLE_SUCCESS,
  REMOVE_ENCOUNTER_TABLE_FAILURE,
  POST_ENCOUNTER_TABLE_BEGIN,
  POST_ENCOUNTER_TABLE_SUCCESS,
  POST_ENCOUNTER_TABLE_FAILURE,
  FETCH_ENCOUNTER_TABLES_BEGIN,
  FETCH_ENCOUNTER_TABLES_SUCCESS,
  FETCH_ENCOUNTER_TABLES_FAILURE,
  EncounterActionTypes
} from './encounters-action-types'

const initialState: EncountersState = {
  tables: [],
  loading: false,
  error: null,
}

function updateOrAdd(currTables: EncounterTable[], incTable: EncounterTable) {
  const newTables = [...currTables]

  let found = false
  for (let i = 0 ; i < newTables.length ; i++) {
    if (newTables[i].id === incTable.id) {
      newTables[i] = incTable
      found = true
      break
    }
  }
  if (!found) newTables.push(incTable)
  return newTables
}

export default (
  state = initialState,
  action: EncounterActionTypes
  ): EncountersState => {
  switch (action.type) {
    case REMOVE_ENCOUNTER_TABLE_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case REMOVE_ENCOUNTER_TABLE_SUCCESS:
      return {
        ...state,
        loading: false,
        tables: state.tables.filter(table => table.id !== action.payload.id)
      }
    case REMOVE_ENCOUNTER_TABLE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      }
    case POST_ENCOUNTER_TABLE_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case POST_ENCOUNTER_TABLE_SUCCESS:
      return {
        ...state,
        loading: false,
        tables: updateOrAdd(state.tables, action.payload.table)
      }
    case POST_ENCOUNTER_TABLE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      }
    case FETCH_ENCOUNTER_TABLES_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_ENCOUNTER_TABLES_SUCCESS:
      return {
        ...state,
        loading: false,
        tables: action.payload.tables
      }
    case FETCH_ENCOUNTER_TABLES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        tables: []
      }
    default:
      return state
  }
}