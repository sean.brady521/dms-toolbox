export const SET_LOGGED_IN = 'SET_LOGGED_IN'
export const SET_LOADING = 'SET_LOADING'
export const SET_ERROR = 'SET_ERROR'
export const LOGOUT = 'LOGOUT'

interface SetLoggedInAction {
  type: typeof SET_LOGGED_IN
  payload: {
    loggedIn: boolean
  }
}

interface SetLoadingAction {
  type: typeof SET_LOADING
  payload: {
    loading: boolean
  }
}

interface SetErrorAction {
  type: typeof SET_ERROR
  payload: {
    error: string
  }
}

export type UserActionTypes = SetLoggedInAction | SetLoadingAction | SetErrorAction