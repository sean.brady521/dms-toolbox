import { ICampaign } from '../../types/campaigns'
import { fetchAll, addOrModifyEntry, removeById } from 'utils/server'
import { Dispatch } from 'redux'
import { StoreState } from 'types/common'

const _removeCampaignBegin = () => ({
  type: 'REMOVE_CAMPAIGN_BEGIN',
})

const _removeCampaignSuccess = (id: string) => ({
  type: 'REMOVE_CAMPAIGN_SUCCESS',
  payload: { id }
})

const _removeCampaignFailure = (error: Error) => ({
  type: 'REMOVE_CAMPAIGN_FAILURE',
  payload: { error }
})

const _postCampaignBegin = () => ({
  type: 'POST_CAMPAIGN_BEGIN',
})

const _postCampaignSuccess = (campaign: Partial<ICampaign>) => ({
  type: 'POST_CAMPAIGN_SUCCESS',
  payload: { campaign }
})

const _postCampaignFailure = (error: Error) => ({
  type: 'POST_CAMPAIGN_FAILURE',
  payload: { error }
})

const _fetchCampaignsBegin = () => ({
  type: 'FETCH_CAMPAIGNS_BEGIN',
})

const _fetchCampaignsSuccess = (campaigns: ICampaign[]) => ({
  type: 'FETCH_CAMPAIGNS_SUCCESS',
  payload: { campaigns }
})

const _fetchCampaignsFailure = (error: Error) => ({
  type: 'FETCH_CAMPAIGNS_FAILURE',
  payload: { error }
})

export const removeCampaign = (id: string) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const onSuccess = () => { 
      dispatch(_removeCampaignSuccess(id))
    }
    const onFailure = (err: any) => dispatch(_removeCampaignFailure(err))
    dispatch(_removeCampaignBegin())
    return removeById('campaign', id, onSuccess, onFailure)
  }
}

export const postCampaign = (campaign: Partial<ICampaign>) => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const onSuccess = () => { 
      dispatch(_postCampaignSuccess(campaign))
    }
    const onFailure = (err: any) => dispatch(_postCampaignFailure(err))
    dispatch(_postCampaignBegin())
    return addOrModifyEntry('campaign', campaign, onSuccess, onFailure)
  }
}

export const fetchCampaigns = () => {
  return (dispatch: Dispatch, getState: () => StoreState) => {
    const { campaigns, user } = getState()

    if (!user.loggedIn) return

    if (campaigns.campaigns.length || getState().campaigns.loading) return // Weve already fetched

    const onSuccess = (campaigns: ICampaign[]) => { 
      dispatch(_fetchCampaignsSuccess(campaigns))
    }
    const onFailure = (campaigns: any) => dispatch(_fetchCampaignsFailure(campaigns))
    dispatch(_fetchCampaignsBegin())
    return fetchAll('campaigns', onSuccess, onFailure)
  }
}
