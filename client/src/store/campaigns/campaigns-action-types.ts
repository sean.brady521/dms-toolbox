import { ICampaign } from 'types/campaigns';

export const REMOVE_CAMPAIGN_BEGIN = 'REMOVE_CAMPAIGN_BEGIN'
export const REMOVE_CAMPAIGN_SUCCESS = 'REMOVE_CAMPAIGN_SUCCESS'
export const REMOVE_CAMPAIGN_FAILURE = 'REMOVE_CAMPAIGN_FAILURE'
export const POST_CAMPAIGN_BEGIN = 'POST_CAMPAIGN_BEGIN'
export const POST_CAMPAIGN_SUCCESS = 'POST_CAMPAIGN_SUCCESS'
export const POST_CAMPAIGN_FAILURE = 'POST_CAMPAIGN_FAILURE'
export const FETCH_CAMPAIGNS_BEGIN = 'FETCH_CAMPAIGNS_BEGIN'
export const FETCH_CAMPAIGNS_SUCCESS = 'FETCH_CAMPAIGNS_SUCCESS'
export const FETCH_CAMPAIGNS_FAILURE = 'FETCH_CAMPAIGNS_FAILURE'

interface ActionFail {
  type: typeof REMOVE_CAMPAIGN_FAILURE | typeof POST_CAMPAIGN_FAILURE | typeof FETCH_CAMPAIGNS_FAILURE
  payload: {
    error: string
  }
}

interface ActionBegin {
  type: typeof REMOVE_CAMPAIGN_BEGIN | typeof POST_CAMPAIGN_BEGIN | typeof FETCH_CAMPAIGNS_BEGIN
}

interface PostCampaignAction {
  type: typeof POST_CAMPAIGN_SUCCESS
  payload: {
    campaign: ICampaign
  }
}

interface RemoveCampaignAction {
  type: typeof REMOVE_CAMPAIGN_SUCCESS
  payload: {
    id: string
  }
}

interface GetCampaignsAction {
  type: typeof FETCH_CAMPAIGNS_SUCCESS
  payload: {
    campaigns: ICampaign[]
  }
}


export type CampaignActionTypes = ActionFail | ActionBegin | PostCampaignAction | RemoveCampaignAction | GetCampaignsAction