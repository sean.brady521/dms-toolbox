import { CampaignsState } from 'types/common'
import { ICampaign } from 'types/campaigns'
import {
  REMOVE_CAMPAIGN_BEGIN,
  REMOVE_CAMPAIGN_SUCCESS,
  REMOVE_CAMPAIGN_FAILURE,
  POST_CAMPAIGN_BEGIN,
  POST_CAMPAIGN_SUCCESS,
  POST_CAMPAIGN_FAILURE,
  FETCH_CAMPAIGNS_BEGIN,
  FETCH_CAMPAIGNS_SUCCESS,
  FETCH_CAMPAIGNS_FAILURE,
  CampaignActionTypes
} from './campaigns-action-types'

const initialState: CampaignsState = {
  campaigns: [],
  loading: false,
  error: null,
}

function updateOrAdd(currCampaigns: ICampaign[], incCampaign: ICampaign) {
  const newCampaigns = [...currCampaigns]

  let found = false
  for (let i = 0 ; i < newCampaigns.length ; i++) {
    if (newCampaigns[i].id === incCampaign.id) {
      newCampaigns[i] = incCampaign
      found = true
      break
    }
  }
  if (!found) newCampaigns.push(incCampaign)
  return newCampaigns
}

export default (
  state = initialState,
  action: CampaignActionTypes
  ): CampaignsState => {
  switch (action.type) {
    case REMOVE_CAMPAIGN_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case REMOVE_CAMPAIGN_SUCCESS:
      return {
        ...state,
        loading: false,
        campaigns: state.campaigns.filter(campaign => campaign.id !== action.payload.id)
      }
    case REMOVE_CAMPAIGN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      }
    case POST_CAMPAIGN_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case POST_CAMPAIGN_SUCCESS:
      return {
        ...state,
        loading: false,
        campaigns: updateOrAdd(state.campaigns, action.payload.campaign)
      }
    case POST_CAMPAIGN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      }
    case FETCH_CAMPAIGNS_BEGIN:
      return {
        ...state,
        loading: true,
        error: null
      }
    case FETCH_CAMPAIGNS_SUCCESS:
      return {
        ...state,
        loading: false,
        campaigns: action.payload.campaigns
      }
    case FETCH_CAMPAIGNS_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        campaigns: []
      }
    default:
      return state
  }
}