import React from 'react'
import ReactDOM from 'react-dom'
import App from 'components/app'
import 'semantic-ui-css/semantic.min.css'
import './index.css'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import configureStore, { history } from './store/configureStore'

const store = configureStore()

const appWrapper = (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>
)

ReactDOM.render(
  appWrapper,
  document.getElementById('root')
)
