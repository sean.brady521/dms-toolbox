import React from 'react'
import { connect } from 'react-redux'
import AccountMenu, { AccountMenuProps } from './account-menu' 
import { StoreState } from 'types/common'
import { getUserEmail } from 'utils/auth'

function mapStateToProps (store: StoreState) {
  return {
    loading: store.user.loading,
    email: getUserEmail()
  }
}

export const AccountMenuContainer = (props: AccountMenuProps) => {
  return (
    <AccountMenu {...props} />
  )
}
export default connect(mapStateToProps)(AccountMenuContainer)