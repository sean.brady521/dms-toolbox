import React from 'react'
import styles from './account-menu.module.scss'
import classNames from 'classnames'
import { Button, Icon, Popup } from 'semantic-ui-react'


export interface AccountMenuProps {
  loggedIn: boolean
  className: string
  loading: boolean
  email?: string | null
  changePage: (page: string) => void
  signOut: () => void
}

const AccountMenu = (props: AccountMenuProps) => {
  const { loggedIn, loading, className, email, signOut, changePage } = props

  if (loading) return <div />

  return (
    <div className={classNames(styles.root, className)}>
      {!loggedIn && 
        <div className={styles.signIn} onClick={() => changePage('login')}>
          Sign In
        </div>
      }
      {loggedIn && 
        <Popup 
          on='click'
          basic
          position='bottom center'
          className={styles.popup}
          trigger={
            <Icon fitted name='user circle' className={styles.userCircle} size='big'/>
          }
        >
          <div className={styles.menuRoot}>
            <span className={styles.label}> Signed in as: </span>
            <span className={styles.email} >{email}</span>
            <Button 
              size='large'
              className={styles.logout} 
              basic
              onClick={signOut}
            >
              Sign out
            </Button>
          </div>
        </Popup> 
      }
    </div>
  )
  
}

export default AccountMenu