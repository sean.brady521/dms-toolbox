import React from 'react'
import { Dispatch } from 'redux'
import { replace } from 'connected-react-router'
import { connect } from 'react-redux'
import parseLocation from '../../utils/parse-location'
import Topbar, { TopbarProps } from './topbar' 
import { StoreState } from 'types/common'
import pages from 'config/pages'
import { signOut } from 'utils/auth'

function mapStateToProps (state: StoreState) {
  const { route } = parseLocation(state.router.location)
  return {
    loggedIn: state.user.loggedIn,
    activeItem: route,
    items: pages,
    signOut
  }
}

function mapDispatchToProps (dispatch: Dispatch) {
  return {
    changePage: (pageName: string) => {
      dispatch(replace(pageName))
    },
  }
}

export const TopbarContainer = (props: TopbarProps) => {
  return (
    <Topbar {...props} />
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(TopbarContainer)
