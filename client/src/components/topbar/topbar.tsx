import React, { useRef, useState } from 'react'
import classNames from 'classnames'

import styles from './topbar.module.scss'
import AccountMenu from './components/account-menu'
import { Page } from 'config/pages'
import { useWindowResize } from 'utils/ui'
import { Icon } from 'semantic-ui-react'

const OVERFLOW_RES = 700

export interface TopbarProps {
  activeItem: string
  loggedIn: boolean
  disabled: boolean
  items: Page[]
  changePage: (page: string) => void
  onSidebarToggle: () => void
  signOut: () => void
}

export default function Topbar(props: TopbarProps) {
  const { activeItem, disabled, loggedIn, items, changePage, onSidebarToggle, signOut } = props

  const mainNavigation = useRef<HTMLDivElement>()
  const [windowRes, setWindowRes] = useState<number>(window.innerWidth)

  const filteredItems = loggedIn
    ? items
    : items.filter(it => !it.requireAuth)

  useWindowResize(() => {
    setWindowRes(window.innerWidth)
  })

  const isOverflow = windowRes < OVERFLOW_RES

  return (
    <div className={styles.root}>
      <div className={classNames(styles.menuItems, disabled && styles.disabled)} ref={mainNavigation as any}>
        {!isOverflow && filteredItems.map(item => (
          <div
            key={item.value}
            className={classNames(styles.menuItem, activeItem === item.value && styles.activeItem)}
            onClick={() => changePage(item.value)}
          >
            {item.label}
          </div>
        ))}
        {isOverflow &&
          <div onClick={onSidebarToggle} className={styles.menuItem}>
            <Icon name='bars' size='large' />
          </div>
        }
        <AccountMenu
          loggedIn={loggedIn}
          className={classNames(styles.menuItem, styles.accountMenu)}
          changePage={changePage}
          signOut={signOut}
        />
      </div>
    </div>
  )
}
