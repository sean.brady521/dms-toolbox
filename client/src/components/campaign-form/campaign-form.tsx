import React, { SyntheticEvent, ChangeEvent} from 'react'
import classNames from 'classnames'
import styles from './campaign-form.module.scss'
import { Modal, Form, TextAreaProps, Dropdown, Icon } from 'semantic-ui-react'
import { ICampaign } from 'types/campaigns'

export interface CampaignFormProps {
  campaign: ICampaign
  open: boolean

  onClose: () => void
  onChange?: (name: string, value: string) => void
  onDelete?: () => void
}

const CampaignForm = (props: CampaignFormProps) => {
  const {
    campaign,

    open,

    onClose,
    onChange,
    onDelete,
  } = props

  const handleInputChange = (_: ChangeEvent, data: any) => {
    const { name, value } = data
    onChange && onChange(name, value)
  }

  const handleTextChange = (e: SyntheticEvent, data: TextAreaProps) => {
    const { name, value } = data
    onChange && onChange(name, `${value || ''}`)
  }

  return (
    <Modal 
      open={open}
      size='small'
      onClose={onClose}
    >
      <div className={styles.root}>
        <div className={styles.topbar}>
          <div onClick={onDelete} className={classNames(styles.menuItem, styles.delete)}>
            <Icon name='trash alternate' className={styles.icon} />
            delete
          </div>
        </div>
        <Form className={styles.form}>
          <Form.Input 
            className={styles.formInput}
            name='name'
            value={campaign.name}
            fluid 
            label='Campaign name'  
            placeholder='Goblins are attacking'
            onChange={handleInputChange}
          />
          <Form.TextArea 
            className={styles.textArea}
            rows={6}
            value={campaign.description}
            label='Description'
            placeholder='A brief description of your campaign...'
            name='description'
            onChange={handleTextChange}
          />
        </Form>
      </div>
    </Modal>
  )
}

export default CampaignForm