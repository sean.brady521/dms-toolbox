import React, { useState } from 'react'
import { Route, Switch } from 'react-router-dom'
import Topbar from 'components/topbar'
import Home from 'routes/home'
import Npcs from 'routes/npcs'
import styles from './app.module.scss'
import Login from 'routes/login'
import FullPageLoad from 'components/full-page-load'
import Campaigns from 'routes/campaigns'
import SpeedRoller from 'routes/speed-roller'
import Encounters from 'routes/encounters'
import {Menu, Segment, Sidebar } from 'semantic-ui-react'
import MainSidebar from './components/main-sidebar'

export interface AppProps {
  loading: boolean
}

const App = (props: AppProps) => {
  const { loading } = props
  const [sidebarOpen, setSidebarOpen] = useState<boolean>(false)

  return (
    <Sidebar.Pushable as={Segment} className={styles.pushable}>
      <Sidebar.Pusher>
        <MainSidebar open={sidebarOpen} onHide={() => setSidebarOpen(false)} />
        <div className={styles.root}>
          <Topbar disabled={loading} onSidebarToggle={() => setSidebarOpen(!sidebarOpen)} />
          <Switch>
            {loading && <Route path='*' component={FullPageLoad} />}
            <Route exact path='/' component={Home} />
            <Route path='/home' component={Home} />
            <Route path='/npcs' component={Npcs} />
            <Route path='/campaigns' component={Campaigns} />
            <Route path='/login' component={Login} />
            <Route path='/speed-roller' component={SpeedRoller} />
            <Route path='/encounters' component={Encounters} />
          </Switch>
        </div>
      </Sidebar.Pusher>
    </Sidebar.Pushable>
  )
}
export default App
