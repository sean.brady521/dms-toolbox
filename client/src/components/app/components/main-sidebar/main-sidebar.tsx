import React from 'react'
import styles from './main-sidebar.module.scss'
import { Menu, Sidebar } from 'semantic-ui-react'
import classNames from 'classnames'
import { Page } from 'config/pages'
import { Icon } from 'semantic-ui-react'

export interface MainSidebarProps {
  open: boolean
  activeItem: string
  loggedIn: boolean
  items: Page[]
  changePage: (page: string) => void
  onHide: () => void
}

const MainSidebar = (props: MainSidebarProps) => {
  const { 
    open, 
    activeItem,
    loggedIn,
    items,
    changePage,
    onHide 
  } = props

  return (
    <Sidebar
      as={Menu} 
      animation='overlay'
      inverted
      onHide={onHide}
      vertical
      className={styles.sidebar}
      visible={open}
    >
      <div className={styles.root}>
        {items.map(item => (
          <div 
            className={styles.routeItem}
            key={item.label}
            onClick={() => { 
              changePage(item.value)
              onHide()
            }}
          >
            {item.label}
          </div>
        ))}
      </div>
    </Sidebar>
  )
}

export default MainSidebar