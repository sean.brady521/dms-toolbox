import React from 'react'
import { Dispatch } from 'redux'
import { replace } from 'connected-react-router'
import { connect } from 'react-redux'
import parseLocation from 'utils/parse-location'
import MainSidebar, { MainSidebarProps } from './main-sidebar' 
import { StoreState } from 'types/common'
import pages from 'config/pages'
import { signOut } from 'utils/auth'

function mapStateToProps (state: StoreState) {
  const { route } = parseLocation(state.router.location)
  return {
    loggedIn: state.user.loggedIn,
    activeItem: route,
    items: pages,
    signOut
  }
}

function mapDispatchToProps (dispatch: Dispatch) {
  return {
    changePage: (pageName: string) => {
      dispatch(replace(pageName))
    },
  }
}

export const MainSidebarContainer = (props: MainSidebarProps) => {
  return (
    <MainSidebar {...props} />
  )
}
export default connect(mapStateToProps, mapDispatchToProps)(MainSidebarContainer)
