import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { StoreState, } from 'types/common'
import App, { AppProps } from './app'
import { initAuthManager } from 'store/user/user-actions'
import { initialiseFirebase } from 'utils/auth'
import { fetchNpcs } from 'store/npcs/npcs-actions'
import { fetchCampaigns } from 'store/campaigns/campaigns-actions'

interface AppContainerProps extends AppProps {
  startAuthManager: () => void
}

export const AppContainer = (props: AppContainerProps) => {
  const { startAuthManager, ...rest } = props

  useEffect(() => {
    initialiseFirebase()
    startAuthManager()
  }, [])

  return <App {...rest} />
}

export function mapStateToProps(state: StoreState) {
  const { loading } = state.user
  return { loading }
}

export function mapDispatchToProps(dispatch: any) {
  const prefetchData = () => {
    dispatch(fetchNpcs())
    dispatch(fetchCampaigns())
  }

  const startAuthManager = async () => {
    dispatch(initAuthManager(prefetchData)) 
  }

  return {
    startAuthManager
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)