import React from 'react'
import styles from './display-card.module.scss'
import classNames from 'classnames'

interface DisplayCardProps {
  imageSrc: string
  title: string
  body: string
  size?: 'small' | 'large'
  className?: string
  comingSoon?: boolean
  moreText?: string
  onMore: () => void
}

const DisplayCard = (props: DisplayCardProps) => {
  const { 
    imageSrc, 
    size = 'large',
    moreText, 
    comingSoon, 
    title, 
    body, 
    className, 
    onMore 
  } = props
  return (
    <div className={classNames(styles.card, className)}>
      <img src={imageSrc} className={size === 'small' ? styles.imageSmall : styles.imageLarge }/>
      <div className={size === 'small' ? styles.cardContentSmall : styles.cardContentLarge}>
        <div className={styles.title}>
          {title}
        </div>
        <div className={styles.body}>
          {body}
        </div>
        {(!!onMore && !!moreText) && 
          <div 
            className={styles.readMore}
            onClick={onMore}
          >
            {moreText} 
          </div>}
        {comingSoon && 
          <div 
            className={styles.comingSoon}
            onClick={onMore}
          >
            Coming Soon
          </div>}
      </div>
    </div>
  )
}

export default DisplayCard