import React from 'react'
import styles from './ability-score.module.scss'
import { Input } from 'semantic-ui-react'

function getModifier (score: number): string {
  const value =  Math.floor((score - 10) / 2)
  return value >= 0 ? `+${value}` : `${value}`
}

interface AbilityScoreProps {
  name: string
  value?: number
  edittable?: boolean
  onChange?: (newVal: string) => void
}

const AbilityScore = (props: AbilityScoreProps) => {
  const { name, value, edittable, onChange } = props

  const handleChange = (value: string) => {
    if (!onChange) return
    const parsedVal = parseInt(value)
    const result = isNaN(parsedVal)
      ? '' 
      : `${Math.min(Math.max(parsedVal, 0), 30)}`
    onChange(result)
  }

  return (
    <div className={styles.root}>
      <img alt='ability-frame' src='https://i.ibb.co/5RYx21j/ability-Frame.png'/> 
      <div className={styles.name}>
        {name}
      </div>
      <div className={styles.value}>
        {edittable 
          ? <Input 
              value={value} 
              className={styles.input}
              onChange={(_, { value }) => handleChange(value)} 
              type='number'
              transparent
            />
          : value
        }
      </div>
      <div className={styles.modifier}>
        {getModifier(value || 0)}
      </div>
    </div>
  )
}

export default AbilityScore