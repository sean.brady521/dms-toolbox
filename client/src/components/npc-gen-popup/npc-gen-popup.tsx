import React, { ReactNode, SyntheticEvent, useState } from 'react'
import styles from './npc-gen-popup.module.scss' 
import { Popup, Dropdown, Button, DropdownProps } from 'semantic-ui-react'
import { Difficulty } from 'constants/enums'
import { GenerateOptions } from 'types/npcs'

export interface NpcGenPopupProps {
  basic?: boolean
  trigger: ReactNode
  races: string[]
  genders: string[]
  plothooks: string[]
  classes: string[]
  socialClasses: string[]
  getProfessions: (socialClass: string) => string[]
  onSubmit: (conf: GenerateOptions) => void
  onClose?: () => void
}

const NpcGenPopup = (props: NpcGenPopupProps) => {
  const { 
    trigger, 
    basic, 
    races,
    genders,
    plothooks, 
    classes, 
    socialClasses,

    getProfessions,

    onSubmit
  } = props

  const [generateOps, setGenerateOps] = useState<GenerateOptions>({})

  const handleOpsChange = (_: SyntheticEvent, data: DropdownProps) => {
    const { name, value }  = data

    const newGenerateOps: GenerateOptions = {...generateOps}
    // @ts-ignore
    newGenerateOps[name] = value
    if (name === 'classorprof')  {
      delete newGenerateOps.occupation1
      delete newGenerateOps.occupation2
    }

    setGenerateOps(newGenerateOps)
  }

  function mapToOps (ops: string[]) {
    const mappedOps = ops.map((op, i) => ({ text: op, value: i }))
    mappedOps.unshift({ text: 'Random', value: -1 })
    return mappedOps
  }


  const difficultyOps = []
  for (const [text, value] of Object.entries(Difficulty)) {
    if (isNaN(parseInt(text))) {
      difficultyOps.push({text: text.toLowerCase(), value})
    }
  }

  return (
    <Popup 
      trigger={trigger}
      basic={basic}
      on='click'
      position='bottom center'
    >
      <div className={styles.root}>
        <div className={styles.line}>
          Race:
          <Dropdown
            name='race'
            className={styles.dropdown}
            scrolling
            options={mapToOps(races)}
            value={typeof generateOps.race === 'number' ? generateOps.race : -1}
            onChange={handleOpsChange}
          />
        </div>
        <div className={styles.line}>
          Gender:
          <Dropdown
            name='gender'
            className={styles.dropdown}
            scrolling
            options={mapToOps(genders)}
            value={typeof generateOps.gender === 'number' ? generateOps.gender : -1}
            onChange={handleOpsChange}
          />
        </div>
        <div className={styles.line}>
          Plot Hook:
          <Dropdown
            name='plothook'
            className={styles.dropdown}
            scrolling
            options={mapToOps(plothooks)}
            value={typeof generateOps.plothook === 'number' ? generateOps.plothook : -1}
            onChange={handleOpsChange}
          />
        </div>
        <div className={styles.line}>
          Class/Prof:
          <Dropdown
            name='classorprof'
            className={styles.dropdown}
            scrolling
            options={mapToOps(['Class', 'Profession'])}
            value={typeof generateOps.classorprof === 'number' ? generateOps.classorprof : -1}
            onChange={handleOpsChange}
          />
        </div>
        {generateOps.classorprof === 0 && 
          <div className={styles.line}>
            Class:
            <Dropdown
              name='occupation1'
              className={styles.dropdown}
              scrolling
              options={mapToOps(classes)}
              value={typeof generateOps.occupation1 === 'number' ? generateOps.occupation1 : -1}
              onChange={handleOpsChange}
            />
          </div>
        }
        {generateOps.classorprof === 1 && 
          <div className={styles.line}>
            Social Class:
            <Dropdown
              name='occupation1'
              className={styles.dropdown}
              scrolling
              options={mapToOps(socialClasses)}
              value={typeof generateOps.occupation1 === 'number' ? generateOps.occupation1 : -1}
              onChange={handleOpsChange}
            />
          </div>
        }
        {(generateOps.classorprof === 1 && typeof generateOps.occupation1 === 'number') &&
          <div className={styles.line}>
            Profession:
            <Dropdown
              name='occupation2'
              className={styles.dropdown}
              scrolling
              options={mapToOps(getProfessions(socialClasses[generateOps.occupation1].toLowerCase()))}
              value={typeof generateOps.occupation2 === 'number' ? generateOps.occupation2 : -1}
              onChange={handleOpsChange}
            />
          </div>
        }
        <div className={styles.buttonWrap}>
          <Button 
            className={styles.button}
            onClick={() => { 
              onSubmit(generateOps)
              setGenerateOps({})
            }}
          >
            Generate
          </Button>
        </div>
      </div>
    </Popup>
  )
}

export default NpcGenPopup