import React  from 'react'
import camelcase from 'camelcase'
import NpcGenPopup, { NpcGenPopupProps } from './npc-gen-popup'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import { getNamedTableOptions, getTableReferenceOptions } from 'utils/npc-generator/npc-data/tables';

export const NpcGenPopupContainer = (props: NpcGenPopupProps) => {
  return (
    <NpcGenPopup {...props} />
  )
}

export function mapStateToProps(_: StoreState) {
  const races: string[] = getTableReferenceOptions('race').map(r => r.name || '')
  const genders = getNamedTableOptions('gender').map(g => g.name || '')
  const plothooks = getNamedTableOptions('hooks').map(p => p.name || '')
  const classes = getNamedTableOptions('class').map(c => c.name || '')
  const socialClasses = getNamedTableOptions('profession').map(c => c.name || '')
  
  const getProfessions = (socialClass: string) => {
    return getNamedTableOptions(camelcase(socialClass)).map(c => c.name || '')
  }
  
  return {
    races,
    genders,
    plothooks,
    socialClasses,
    classes,
    getProfessions
  }
}

export default connect(mapStateToProps)(NpcGenPopupContainer)