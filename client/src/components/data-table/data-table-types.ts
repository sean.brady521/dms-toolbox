import { ReactNode } from "react";

export type DataCell = string | number | ReactNode