import React, { useState } from 'react'
import { Table, Icon } from 'semantic-ui-react'
import classNames from 'classnames'

import { DataCell } from './data-table-types'
import { ReactComponent as DiceIcon } from 'assets/icons/dice.svg'

import styles from './data-table.module.scss'
import { EncounterConfig } from 'types/encounters'
import ConfigPopup from 'components/config-popup'

interface DataTableProps {
  className?: string
  headers: string[]
  rows: DataCell[][]
  rowsDeletable?: boolean
  defaultEncounterConfig: EncounterConfig
  onNewRow: () => void
  onDeleteRow: (rowIdx: number) => void
  onRandomiseRow: (rowIdx: number, config: EncounterConfig) => void
}


const DataTable = (props: DataTableProps) => {
  const { headers, rows, defaultEncounterConfig, className, onNewRow, onDeleteRow, onRandomiseRow } = props
  const [confPopupOpen, setConfPopupOpen] = useState<number | null>(null)

  const transformedHeaders = [...headers, '']

  return (
    <div className={classNames(styles.root, className)}>
      <Table striped>
        <Table.Header>
          <Table.Row>
            {transformedHeaders.map(headerCol => (
              <Table.HeaderCell key={headerCol}>{headerCol}</Table.HeaderCell>
            ))}
          </Table.Row>
        </Table.Header>
        <Table.Body >
          {rows.map((row, idx) => (
            <Table.Row key={`row-${idx}`} className={styles.row}>
              {row.map((cell, idx) => (
                <Table.Cell key={`cell-${idx}`}>{cell}</Table.Cell>
              ))}
              <Table.Cell>
                <div className={styles.iconWrappers}>
                  <ConfigPopup 
                    open={idx === confPopupOpen}
                    onClose={() => setConfPopupOpen(null)}
                    defaultConfig={defaultEncounterConfig}
                    trigger={(<DiceIcon onClick={() => setConfPopupOpen(idx)} className={styles.dice} />)} 
                    onSubmit={conf => { 
                      onRandomiseRow(idx, conf) 
                      setConfPopupOpen(null)
                    }}
                  />
                  <Icon 
                    name='trash alternate' 
                    className={styles.trash} 
                    onClick={() => onDeleteRow(idx)}
                  />
                </div>
              </Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
        <Table.Footer 
          className={styles.addNew}
          onClick={onNewRow}
        >
          <Table.Row>
            <Table.Cell>
              <Icon name='add' />
              New
            </Table.Cell>
            {transformedHeaders.slice(1).map((_, idx) => (
              <Table.Cell key={`empty-${idx}`}/>
            ))}
          </Table.Row>
        </Table.Footer>
      </Table>
    </div>
  )
}

export default DataTable