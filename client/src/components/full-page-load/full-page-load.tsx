import React from 'react'
import { Loader, Dimmer } from 'semantic-ui-react'
import styles from './full-page-load.module.scss'

const FullPageLoad = () => {
  return (
    <div className={styles.root}>
      <Loader active>Loading</Loader>
    </div>
  )
}

export default FullPageLoad
