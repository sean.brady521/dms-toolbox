import React, { ReactNode } from 'react'
import classNames from 'classnames'
import { Option } from 'types/common'
import styles from './hover-select.module.scss'

interface CSSOverrides {
  menu?: string
}

interface HoverSelectProps {
  button: ReactNode
  className?: string
  classes?: CSSOverrides
  options: Option<string>[]
  onSelect: (value: string) => void 
}

const HoverSelect = (props: HoverSelectProps) => {
const { button, className, classes = {}, options, onSelect } = props

  return (
    <div 
      className={classNames(styles.root, className)}
    >
      <div className={styles.button}>
        {button}
      </div>
      <div className={classNames(styles.menu, classes.menu)}>
        {options.map(op => (
          <div 
            key={op.value} 
            onClick={() => onSelect(op.value)}
            className={classNames(styles.item)}
          >
            {op.label}
          </div>
        ))}
      </div>
    </div>
  )
}

export default HoverSelect