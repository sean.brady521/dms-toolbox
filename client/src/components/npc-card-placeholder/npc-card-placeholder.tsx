import React from 'react'
import styles from './npc-card-placeholder.module.scss'
import { Icon } from 'semantic-ui-react'
import classNames from 'classnames'


interface NpcCardPlaceholderProps {
  className?: string
  onClick?: () => void
}

const NpcCardPlaceholder = (props: NpcCardPlaceholderProps) => {
  const {
    className,
    onClick
  } = props

  return (
    <div 
      className={classNames(styles.root, className, styles.hover)}
      onClick={onClick}
    >
      <Icon name='add' size='large' />
    </div>
  )
}

export default NpcCardPlaceholder