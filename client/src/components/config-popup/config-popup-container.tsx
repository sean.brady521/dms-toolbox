import React, { useEffect } from 'react'
import ConfigPopup, { ConfigPopupProps } from './config-popup'
import { connect } from 'react-redux'
import { EncounterConfig } from 'types/encounters'
import { StoreState } from 'types/common'

export const ConfigPopupContainer = (props: ConfigPopupProps) => {
  return (
    <ConfigPopup {...props} />
  )
}

export function mapStateToProps(_: StoreState, ownProps: ConfigPopupProps) {
  const storageConf = localStorage.getItem('encounterConfig')
  const defaultConfig = storageConf ? JSON.parse(storageConf) : undefined
  return {
    defaultConfig,
    onSubmit: (config: EncounterConfig) => {
      localStorage.setItem('encounterConfig', JSON.stringify(config))
      ownProps.onSubmit(config)
    }
  }
}

export default connect(mapStateToProps)(ConfigPopupContainer)