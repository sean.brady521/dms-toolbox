import React, { ReactNode, useState } from 'react'
import styles from './config-popup.module.scss' 
import { Popup, Dropdown, Button } from 'semantic-ui-react'
import { EncounterConfig } from 'types/encounters'
import { Difficulty, Environment } from 'constants/enums'

export interface ConfigPopupProps {
  basic?: boolean
  trigger: ReactNode
  open?: boolean
  onSubmit: (conf: EncounterConfig) => void
  onClose?: () => void
  defaultConfig?: EncounterConfig
}

const ConfigPopup = (props: ConfigPopupProps) => {
  const { trigger, basic, open, defaultConfig, onSubmit, onClose } = props

  const [avgLvl, setAvgLvl] = useState(defaultConfig?.avgLvl || 1)
  const [numPcs, setNumPcs] = useState(defaultConfig?.numPcs || 3)
  const [difficulty, setDifficulty] = useState(defaultConfig?.difficulty || Difficulty.Medium)
  const [environment, setEnvironment] = useState(defaultConfig?.environment || Environment.City)

  const difficultyOps = []
  for (const [text, value] of Object.entries(Difficulty)) {
    if (isNaN(parseInt(text))) {
      difficultyOps.push({text: text.toLowerCase(), value})
    }
  }

  return (
    <Popup 
      trigger={trigger}
      basic={basic}
      on='click'
      open={open}
      onClose={onClose}
      position='bottom center'
    >
      <div className={styles.root}>
        <div className={styles.line}>
          Average PC level:
          <Dropdown
            className={styles.dropdown}
            scrolling
            options={[...Array(20).keys()].map(lvl => ({ text: `${lvl + 1}`, value: lvl + 1 }))}
            value={avgLvl}
            onChange={(_, { value }) => setAvgLvl(value as number)}
          />
        </div>
        <div className={styles.line}>
          Number of PCs
          <Dropdown
            className={styles.dropdown}
            scrolling
            options={[...Array(10).keys()].map(num => ({ text: `${num + 1}`, value: num + 1 }))}
            value={numPcs}
            onChange={(_, { value }) => setNumPcs(value as number)}
          />
        </div>
        <div className={styles.line}>
          Environment
          <Dropdown
            className={styles.dropdown}
            options={Object.values(Environment).map(dif => ({text: dif, value: dif}))}
            onChange={(_, { value }) => setEnvironment(value as Environment)}
            value={environment}
          />
        </div>
        <div className={styles.line}>
          Difficulty
          <Dropdown
            className={styles.dropdown}
            options={difficultyOps}
            value={difficulty}
            onChange={(_, { value }) => setDifficulty(value as Difficulty)}
          />
        </div>
        <div className={styles.buttonWrap}>
          <Button 
            className={styles.button}
            onClick={() => onSubmit({ avgLvl, numPcs, difficulty, environment })}
          >
            Generate
          </Button>
        </div>
      </div>
    </Popup>
  )
}

export default ConfigPopup