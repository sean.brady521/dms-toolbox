import React from 'react'
import NpcForm, { NpcFormProps } from './npc-form'
import { connect } from 'react-redux'
import { StoreState } from 'types/common'
import {getNamedTableOptions, getTableReferenceOptions } from 'utils/npc-generator/npc-data/tables';

export const NpcFormContainer = (props: NpcFormProps) => {
  return (
    <NpcForm {...props} />
  )
}

export function mapStateToProps(store: StoreState) {
  const races: string[] = getTableReferenceOptions("race").map(r => r.name || '')
  const genders = getNamedTableOptions("gender").map(g => g.name || '')
  const plothooks = getNamedTableOptions("hooks").map(p => p.name || '')
  const classes = getNamedTableOptions("class").map(c => c.name || '')
  return {
    races,
    genders,
    plothooks,
    classes,
    loggedIn: store.user.loggedIn
  }
}


export default connect(mapStateToProps)(NpcFormContainer)