import React, {useState, SyntheticEvent, ChangeEvent} from 'react'
import classNames from 'classnames'
import styles from './npc-form.module.scss'
import { Modal, Form, Dropdown, Button, Icon, Input, TextArea, TextAreaProps, InputOnChangeData } from 'semantic-ui-react'
import { INpc, GenerateOptions } from 'types/npcs'

import { ReactComponent as DiceIcon } from 'assets/icons/dice.svg'
import AbilityScore from 'components/ability-score'
import { ICampaign } from 'types/campaigns'
import NpcGenPopup from 'components/npc-gen-popup'

export interface NpcFormProps {
  readOnly?: boolean
  open: boolean
  loggedIn: boolean

  npc: Partial<INpc>
  campaigns: ICampaign[]
  races: string[]
  genders: string[]
  plothooks: string[]
  classes: string[]
  generateSuccessful?: boolean

  onClose: () => void
  onChange?: (name: string, value: string) => void
  onDelete?: () => void
  onGenerate?: (options?: GenerateOptions) => void
}

const NpcForm = (props: NpcFormProps) => {
  const {
    open,
    readOnly,
    loggedIn,

    npc,
    campaigns,
    generateSuccessful,

    onClose,
    onChange,
    onDelete,
    onGenerate,
  } = props

  const handleTextChange = (_: any, data: TextAreaProps | InputOnChangeData) => {
    const { name, value } = data
    onChange && onChange(name, `${value || ''}`)
  }

  const campaignOps = [ 
    ...campaigns.map(c => ({ text: c.name || 'Untitled', value: c.id })),
    { text: 'all', value: 'all' }
  ]

  return (
    <Modal 
      open={open}
      size='large'
      className={styles.modal}
      onClose={onClose}
    >
      <div className={styles.root}>
        <div className={styles.topbar}>
          {!readOnly && 
            <>
              <div className={styles.menuItem}>
                <NpcGenPopup 
                  trigger={(
                    <div className={styles.generateWrap}>
                      <DiceIcon className={styles.icon} />
                      generate
                    </div>
                  )}
                  onSubmit={ops => { 
                    onGenerate && onGenerate(ops)
                  }}
                />
              </div>
              <div onClick={onDelete} className={classNames(styles.menuItem, styles.delete)}>
                <Icon name='trash alternate' className={styles.icon} />
                delete
              </div>
            </>
          }
        </div>
        <div className={styles.formWrapper}>
          <Form className={styles.form}>
            <Form.Group widths='equal' className={styles.formGroup}>
              <Form.Field 
                fluid
              >
                <label className={styles.formLabel}>Context</label>
                <Input 
                  placeholder='E.g. Guy from the tavern' 
                  name='context'
                  value={npc.context}
                  onChange={handleTextChange}
                  readOnly={readOnly}
                />
              </Form.Field>
              <Form.Field 
                fluid
              >
                <label className={styles.formLabel}>Campaign</label>
                <Dropdown 
                  placeholder='Campaign' 
                  disabled={!loggedIn}
                  options={campaignOps}
                  onChange={(_, { value }) => onChange && onChange('campaign', value as string)}
                  value={npc.campaign || 'all'}
                  selection
                  readOnly={readOnly}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths='equal' className={styles.formGroup}>
              <Form.Field 
                fluid
              >
                <label className={styles.formLabel}>Name</label>
                <Input 
                  placeholder='Gerrard Thunderseaker' 
                  name='name'
                  value={npc.name}
                  onChange={handleTextChange}
                  readOnly={readOnly}
                />
              </Form.Field>
              <Form.Field 
                fluid
              >
                <label className={styles.formLabel}>Alignment</label>
                <Input 
                  placeholder='Lawful good' 
                  name='alignment'
                  value={npc.alignment}
                  onChange={handleTextChange}
                  readOnly={readOnly}
                />
              </Form.Field>
            </Form.Group>
            <Form.Group widths='equal' className={styles.formGroup}>
              <Form.Field 
                fluid
              >
                <label className={styles.formLabel}>Description</label>
                <TextArea 
                  className={styles.textArea} 
                  name='description'
                  value={npc.description}
                  onChange={handleTextChange}
                  placeholder='Background and phsyical characteristics...' 
                  readOnly={readOnly}
                />
              </Form.Field>
              <Form.Field 
                fluid
              >
                <label className={styles.formLabel}>Personality Traits</label>
                <TextArea 
                  className={styles.textArea} 
                  name='personality'
                  value={npc.personality}
                  onChange={handleTextChange}
                  readOnly={readOnly}
                  placeholder='Religion and personality quirks...' />
              </Form.Field>
            </Form.Group>
            <Form.Group widths='equal' className={styles.formGroup}>
              <Form.Field 
                fluid
              >
                <label className={styles.formLabel}>Plot Hook</label>
                <TextArea 
                  name='hook'
                  rows={3}
                  value={npc.hook}
                  onChange={handleTextChange}
                  readOnly={readOnly}
                  placeholder='Religion and personality quirks...' />
              </Form.Field>
            </Form.Group>
            <div className={styles.voiceAndStats}>
              <div className={styles.statWrap}>
                <div className={styles.formLabel}>
                  Ability Scores
                </div>
                <div className={styles.stats}>
                  <AbilityScore 
                    name='strength' 
                    value={npc.abilities?.str || 10} 
                    edittable
                    onChange={newVal => onChange && onChange('abilities.str', newVal)}
                  />
                  <AbilityScore 
                    name='dexterity' 
                    value={npc.abilities?.dex || 10} 
                    edittable
                    onChange={newVal => onChange && onChange('abilities.dex', newVal)}
                  />
                  <AbilityScore 
                    name='consitution' 
                    value={npc.abilities?.con || 10} 
                    edittable
                    onChange={newVal => onChange && onChange('abilities.con', newVal)}
                  />
                  <AbilityScore 
                    name='intelligence' 
                    value={npc.abilities?.int || 10} 
                    edittable
                    onChange={newVal => onChange && onChange('abilities.int', newVal)}
                  />
                  <AbilityScore 
                    name='wisdom' 
                    value={npc.abilities?.wis || 10} 
                    edittable
                    onChange={newVal => onChange && onChange('abilities.wis', newVal)}
                  />
                  <AbilityScore 
                    name='charisma' 
                    value={npc.abilities?.cha || 10} 
                    edittable
                    onChange={newVal => onChange && onChange('abilities.cha', newVal)}
                  />
                </div>
              </div>
              <div className={styles.voiceWrap}>
                <div className={styles.formLabel}>
                  Voice Characteristics
                </div>
                <div className={styles.voice}>
                  <Form.Input 
                    fluid
                    name='voice.pace'
                    value={npc.voice?.pace}
                    readOnly={readOnly}
                    onChange={handleTextChange}
                    label='Pace' 
                  />
                  <Form.Input 
                    name='voice.pitch'
                    value={npc.voice?.pitch}
                    readOnly={readOnly}
                    onChange={handleTextChange}
                    fluid
                    label='Pitch' 
                  />
                  <Form.Input 
                    name='voice.tone'
                    value={npc.voice?.tone}
                    readOnly={readOnly}
                    onChange={handleTextChange}
                    fluid
                    label='Tone' 
                  />
                  <Form.TextArea 
                    name='voice.physicalQuirk'
                    value={npc.voice?.physicalQuirk}
                    readOnly={readOnly}
                    className={styles.voiceTextArea}
                    rows={2}
                    onChange={handleTextChange}
                    fluid
                    label='Physical Quirk' 
                  />
                  <Form.TextArea 
                    name='voice.speechQuirk'
                    value={npc.voice?.speechQuirk}
                    readOnly={readOnly}
                    className={styles.voiceTextArea}
                    onChange={handleTextChange}
                    rows={2}
                    fluid
                    label='Speech Quirk' 
                  />
                </div>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </Modal>
  )
}

export default NpcForm