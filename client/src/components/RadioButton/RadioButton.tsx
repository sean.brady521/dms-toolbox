import React from 'react'
import classNames from 'classnames'
import styles from './RadioButton.module.scss'

interface RadioButtonProps {
  className?: string
  selected?: boolean
  label?: string
  onClick?: () => void
}

const RadioButton = (props: RadioButtonProps) => {
  const { className, selected, label, onClick } = props

  return (
    <div
      className={classNames(styles.root, className)}
      onClick={onClick}
    >
      <span
        className={classNames(styles.button, selected && styles.selected)}
      />
      {label &&
        <span className={styles.label}>
          {label}
        </span>}
    </div>
  )
}

export default RadioButton
