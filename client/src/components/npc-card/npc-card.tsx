import React from 'react'
import styles from './npc-card.module.scss'
import NpcCardPlaceholder from './npc-card-placeholder'
import classNames from 'classnames'


interface NpcCardProps {
  className?: string
  placeholder?: boolean

  imageUrl?: string
  name?: string
  title?: string
  context?: string
  pace?: string
  pitch?: string
  tone?: string
  speechQuirk?: string
  physicalQuirk?: string

  onView?: () => void
  onEdit?: () => void
  onDuplicate?: () => void
  onDelete?: () => void


}

const NpcCard = (props: NpcCardProps) => {
  const {
    className,
    placeholder,
    imageUrl,
    title,
    name,
    context,

    onView,
    onEdit,
    onDuplicate,
    onDelete
  } = props

  if (placeholder) return <NpcCardPlaceholder className={className} />

  return (
    <div 
      className={classNames(styles.root, className)}
      onClick={onEdit}
    >
      <div className={styles.infoWrap}>
        <div className={styles.backgroundImage} />
        <div className={styles.meta}>
          <div className={styles.name}>
            {name || 'No Name'}
          </div>
          <div className={styles.context}>
            {context || title || 'No Context'}
          </div>
        </div>
      </div>
      <div className={styles.actions}>
        <div 
          className={classNames(styles.action)} 
          onClick={event => {
            event.stopPropagation()
            onView && onView()
          }}
        >
          view 
        </div>
        <div 
          className={classNames(styles.action)} 
          onClick={event => {
            event.stopPropagation()
            onEdit && onEdit()
          }}
        >
          edit 
        </div>
        <div 
          className={classNames(styles.action)} 
          onClick={event => {
            event.stopPropagation()
            onDuplicate && onDuplicate()
          }}
        >
          duplicate 
        </div>
        <div 
          className={classNames(styles.action, styles.delete)} 
          onClick={event => {
            event.stopPropagation()
            onDelete && onDelete()
          }}
         >
          delete 
        </div>
      </div>
    </div>
  )
}

export default NpcCard