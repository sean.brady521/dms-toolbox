import React from 'react'
import styles from './npc-card.module.scss'
import classNames from 'classnames'
import { Dimmer, Loader } from 'semantic-ui-react'


interface NpcCardPlaceholderProps { className?: string }

const NpcCardPlaceholder = (props: NpcCardPlaceholderProps) => {
  const { className } = props

  return (
    <div 
      className={classNames(styles.root, className, styles.disabled)}
    >
      <div className={styles.infoWrap}>
        <div className={styles.backgroundImage} />
        <Dimmer active>
          <Loader size='medium' />
        </Dimmer>

      </div>
      <div className={styles.actions}>
        <div className={classNames(styles.action)} >
          view 
        </div>
        <div className={classNames(styles.action)} >
          edit 
        </div>
        <div className={classNames(styles.action)} >
          duplicate 
        </div>
        <div className={classNames(styles.action, styles.delete)} >
          delete 
        </div>
      </div>
    </div>
  )
}

export default NpcCardPlaceholder