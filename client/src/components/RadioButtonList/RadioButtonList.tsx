import React from 'react'
import RadioButton from '../RadioButton'
import { Option } from 'types/common'
import styles from './RadioButtonList.module.scss'

interface RadioButtonListProps<T> {
  selected?: T
  options: Option<T>[]
  onChange?: (value: T) => void
}

const RadioButtonList = <T extends unknown>(props: RadioButtonListProps<T>) => {
  const { selected, options, onChange } = props

  return (
    <div className={styles.root}>
      {options.map(op => (
        <RadioButton
          key={op.label}
          label={op.label}
          selected={op.value === selected}
          className={styles.button}
          onClick={() => onChange && onChange(op.value)}
        />
      ))}
    </div>
  )
}

export default RadioButtonList
