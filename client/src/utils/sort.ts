/*
  creates a sort predicate using a function to map a value to a string
*/
export function alphaSortFn (func: (val: any) => string): (a: string, b: string) => number {
  return (a: string, b: string) => {
    const aField = func(a).toLowerCase()
    const bField = func(b).toLowerCase()

    if (aField > bField) {
      return 1
    }

    if (aField < bField) {
      return -1
    }

    return 0
  }
}

/*
  creates a sort predicate using a key to map an object to a string
*/
export function alphaSort (key: string): (a: any, b: any) => number {
  // @ts-ignore
  return alphaSortFn((obj: object) => obj[key] || '')
}
