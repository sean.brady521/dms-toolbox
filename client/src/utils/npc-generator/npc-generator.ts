import { generate } from './npc-data/generate'
import { generateVoice } from './voice-generator'
import { generateImage } from './image-generator'
import { INpc, GenerateOptions } from 'types/npcs'
import { combineDescription, combinePersonalityTraits } from './paragraph-creator'
import { deepClone } from 'utils/clone'
const { v4: uuidv4 } = require('uuid')


export interface AlignmentDetailed {
  chaotic: number
  ethicalneutral: number
  evil: number
  good: number
  lawful: number
  moralneutral: number
}

function getAlignmentStr (alignment: AlignmentDetailed): string {
  // @ts-ignore
  const firstPart = ['chaotic', 'ethicalneutral', 'lawful'].sort((a, b) => parseInt(alignment[a]) - parseInt(alignment[b]))[0]
  // @ts-ignore
  const secondPart = ['good', 'evil', 'moralneutral'].sort((a, b) => parseInt(alignment[a]) - parseInt(alignment[b]))[0]
  return `${firstPart} ${secondPart}`.replace('ethical', '').replace('moral', '')

}

export function extractTitle(npc: INpc): string {
  const rgx = /is a \d+ year old (\w+) (\w+) (\w+)\./
  const match = npc.description ? npc.description.match(rgx) || [] : []

  const gender = npc.gender || match[1]
  const race = npc.race || match[2]
  const classorprof = npc.class || match[3]

  let title = ''
  if (gender) title += gender + ' '
  if (race) title += race + ' '
  if (classorprof) title += classorprof

  return title 
}

export function generateRandomNpc (options: GenerateOptions): Partial<INpc> {
  let generated = generate(options).npc

  const alignment = getAlignmentStr(generated.alignment) 
  const name = generated.description.name
  const hook = generated.hook.description
  const description = combineDescription(generated)
  const personality = combinePersonalityTraits(generated)
  const gender = generated.description.gender
  const race = generated.description.race
  const occupationOrClass = generated.description.occupation

  return {
    name,
    alignment,
    imageUrl: generateImage({ gender }),
    voice: generateVoice(),
    gender,
    class: occupationOrClass,
    race,
    description,
    personality,
    hook,
    abilities: generated.abilities
  }
}

export function npcHasData (npc: INpc): boolean {
  const hasData = !isBlank(npc.name) ||
    !isBlank(npc.alignment) ||
    !isBlank(npc.voice) ||
    !isBlank(npc.description) ||
    !isBlank(npc.personality) ||
    !isBlank(npc.abilities)
  return hasData
}

export function isBlank (ob: any): boolean {
  let obIsBlank = true
  if (typeof ob === 'string') {
    obIsBlank = ob === ''
  } else if (typeof ob === 'object' && Array.isArray(ob)) {
    obIsBlank = (ob.length === 0 || ob.every(el => (!el && el !== 0)))
  } else if (typeof ob === 'object') {
    obIsBlank = Object.values(ob).every(isBlank)
  } else {
    obIsBlank = !ob && ob !== 0
  }
  return obIsBlank
}

export function fillInTheBlanks (npc: INpc, incoming: Partial<INpc>): INpc {
  const filledNpc = {...npc}
  for (const [key, val] of Object.entries(incoming)) {
    // @ts-ignore
    if (isBlank(filledNpc[key])) {
      // @ts-ignore
      filledNpc[key] = val
    }
  }
  return filledNpc
}

export function randomiseNpc (npc: INpc, options: GenerateOptions, overwrite: boolean = false): INpc {
  const generatedAttribs = generateRandomNpc(options)
  if (overwrite) {
    return {...npc, ...generatedAttribs}
  } else {
    return fillInTheBlanks(npc, generatedAttribs)
  }
}

export function generateNewNpc (uid?: string, campaign: string = 'all', randomise?: boolean): INpc {
  let baseNpc = {
    imageUrl: generateImage(),
    modifiedAt: Date.now(),
    createdAt: Date.now(),
    createdBy: uid ? uid : 'guest',
    id: uuidv4(),
    campaign
  }
  return randomise ? {...baseNpc, ...generateRandomNpc({})} : baseNpc
}

export function duplicateNpc (npc: INpc): INpc {
  const now = Date.now()
  const clonedNpc = deepClone<INpc>(npc)
  return {...clonedNpc, modifiedAt: now, createdAt: now, id: uuidv4()}
}
