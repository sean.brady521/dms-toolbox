import { randSelect } from "utils/rand"

export const imageOptions = [
  { 
    gender: 'male',
    src: 'https://react.semantic-ui.com/images/avatar/large/matthew.png',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar/large/elliot.jpg',
  },
  { 
    gender: 'female',
    src: 'https://semantic-ui.com/images/avatar/large/jenny.jpg',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar/large/joe.jpg',
  },
  { 
    gender: 'female',
    src: 'https://semantic-ui.com/images/avatar/large/helen.jpg',
  },
  { 
    gender: 'female',
    src: 'https://semantic-ui.com/images/avatar/large/ade.jpg',
  },
  { 
    gender: 'female',
    src: 'https://semantic-ui.com/images/avatar2/large/kristy.png',
  },
  { 
    gender: 'female',
    src: 'https://i.pinimg.com/originals/60/7f/f2/607ff22d230565a215aacbe900fba2c2.jpg',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar/large/christian.jpg',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar/large/justen.jpg',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar/large/daniel.jpg',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar/large/steve.jpg',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar/large/chris.jpg',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar2/large/patrick.png',
  },
  { 
    gender: 'female',
    src: 'https://semantic-ui.com/images/avatar2/large/molly.png',
  },
  { 
    gender: 'female',
    src: 'https://react.semantic-ui.com/images/avatar/large/rachel.png',
  },
  { 
    gender: 'male',
    src: 'https://semantic-ui.com/images/avatar2/large/mark.png'
  },
]

interface FilterOptions {
  gender?: string
}

export function generateImage (filters: FilterOptions = {}): string {
  let images = imageOptions
  for (const [key, value] of Object.entries(filters)) {
    // @ts-ignore
    images = images.filter(im => im[key] === value)
  }
  return randSelect(images.map(im => im.src))
}

export function prevImage (imageUrl: string): string {
    const currentIdx = imageOptions.findIndex(img => img.src === imageUrl)
    let newIdx = currentIdx - 1

    if (newIdx < 0) {
      newIdx = imageOptions.length - 1
    }

    return imageOptions[newIdx].src
}

export function nextImage (imageUrl: string): string {
    const currentIdx = imageOptions.findIndex(img => img.src === imageUrl)
    let newIdx = currentIdx + 1

    if (newIdx >= imageOptions.length) {
      newIdx = 0
    }

    return imageOptions[newIdx].src
}