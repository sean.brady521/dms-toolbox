import { Difficulty } from "constants/enums";

export function getDifficultyLabel (difficulty: Difficulty) {
  switch (difficulty) {
    case Difficulty.Easy:
      return 'Easy'
    case Difficulty.Medium:
      return 'Medium'
    case Difficulty.Difficult:
      return 'Difficult'
    case Difficulty.Deadly: 
      return 'Deadly'
  }
}