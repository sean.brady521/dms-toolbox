import { EncounterTable, IEncounter, Personality, Relationship } from "types/encounters";
import { randSelect } from "utils/rand";
import { personalities, relationships } from "./constants";

export function selectFromTable (table: EncounterTable): IEncounter {
  const selectables = []
  for (const row of table.rows) {
    for (let i = 0; i < row.liklihood; i++) {
      selectables.push(row)
    }
  }
  console.log({ selectables })
  const row = randSelect(selectables)

  const personality = personalities.find(p => p.type === row.personality)
  const relationship = relationships.find(r => r.type === row.relationship)

  return {
    creatures: row.creatures,
    personality: personality ? personalityToString(personality) : 'No Personality',
    relationship: relationship ? relationshipToString(relationship) : 'No Relationships',
  }
}

export function personalityToString (pers: Personality) {
  return `${pers.type}; ${pers.description}`
}

export function relationshipToString (rel: Relationship, monster?: string) {
  return `One ${monster || 'enemy'} is ${rel.joiner}${rel.type}, ${rel.description}`
}