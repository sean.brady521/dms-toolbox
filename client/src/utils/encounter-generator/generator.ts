import { Difficulty, Environment, Likihood } from "constants/enums";
import monsters from './monsters.json'
import { randSelect } from "utils/rand";
import { thresholds, personalities, relationships } from './constants'
import { IEncounter, EncounterRow } from "types/encounters";
import { personalityToString, relationshipToString } from "./tables";

export interface Monster {
  creature: string
  environment: string
  type: string
  page: number
  xp: number
}

function determineXp (numPcs: number, avgLvl: number, difficulty: Difficulty) {
  return thresholds[(avgLvl - 1)][(difficulty - 1)] * numPcs
}


function encounterGen (monsterList: Monster[], xpThreshold: number): Monster[] {
  const xpLowerLimit = Math.round(xpThreshold / 25)
  let xpMonsters = 0
  let monsterCount = 0
  const encounteredMonsters: Monster[] = []
  while (xpMonsters < (xpThreshold - (3 * xpLowerLimit))) {
    const possibleMonsters = []
    for (const mon of monsterList) {
      if (mon.xp > xpLowerLimit && mon.xp < (xpThreshold - xpMonsters)) {
        possibleMonsters.push(mon)
      }
    }
    if (!possibleMonsters.length) {
      console.warn('Ran out of suitable monsters')
      return encounteredMonsters
    }
    encounteredMonsters.push(randSelect(possibleMonsters))
    monsterCount = encounteredMonsters.length
    xpMonsters = 0
    for (const mon of encounteredMonsters) {
      xpMonsters += mon.xp
    }

    let countMultplier = 1
    if (monsterCount === 2) countMultplier = 1.5
    else if (monsterCount >= 3 && monsterCount <= 6)  countMultplier = 2
    else if (monsterCount >= 7 && monsterCount <= 10)  countMultplier = 2.5

    xpMonsters *= countMultplier
  }
  return encounteredMonsters
}

function filterMonsters (monsterList: Monster[], environment: Environment): Monster[] {
  return monsterList.filter(mon => mon.environment === environment)
} 

export function _generate (numPcs: number, avgLvl: number, difficulty: Difficulty, environment: Environment)  {
  const xp = determineXp(numPcs, avgLvl, difficulty)
  const filteredMonsters = filterMonsters(monsters, environment)
  const encounteredMonsters = encounterGen(filteredMonsters, xp)

  const uniqueMonsters: { [creature: string]: number } = {}
  for (const mon of encounteredMonsters) {
    if (!uniqueMonsters[mon.creature]) uniqueMonsters[mon.creature] = 0
    uniqueMonsters[mon.creature] += 1
  }
  const encounterStrings = []
  for (const [creature, count] of Object.entries(uniqueMonsters)) {
    encounterStrings.push(`${count} ${creature}(s)`)
  }

  const personality = randSelect(personalities)
  const relationship = randSelect(relationships)

  return {
    creatures: encounterStrings.join('; '),
    personality,  
    relationship,
    uniqueMonsters
  }

}

export function generateEncounterRow (
    numPcs: number, 
    avgLvl: number, 
    difficulty: Difficulty, 
    environment: Environment, 
    liklihood: Likihood
  ): EncounterRow  {
  const { creatures, personality, relationship } = _generate(numPcs, avgLvl, difficulty, environment)
  return {
    creatures,
    personality: personality.type,
    relationship: relationship.type,
    liklihood
  }
}

export function generateEncounter (numPcs: number, avgLvl: number, difficulty: Difficulty, environment: Environment): IEncounter {
  const { creatures, personality, relationship, uniqueMonsters } = _generate(numPcs, avgLvl, difficulty, environment)

  const totalMonsters = Object.values(uniqueMonsters).reduce((a,b) => a + b, 0)

  const personalityStr = personalityToString(personality)

  const randomMonster = randSelect(Object.keys(uniqueMonsters))
  const relationshipStr = totalMonsters > 1 
    ? relationshipToString(relationship, randomMonster)
    : 'No Relationship'

  return  {
    creatures,
    personality: personalityStr,
    relationship: relationshipStr
  }
}