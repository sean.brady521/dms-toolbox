import { useEffect, useState } from "react";

export function mapToOps (ops: string[]) {
  return ops.map(op => ({ text: op, value: op }))
} 

// Hook
export function useWindowResize(onResize: () => void) {
  useEffect(() => {
    window.addEventListener("resize", onResize);
    onResize();
    return () => window.removeEventListener("resize", onResize);
  }, [])
}