import getEndpointBase from '../config/endpoint'
import { getIdToken } from './auth';

// Handle HTTP errors since fetch won't.
function _handleErrors(response: Response) {
  if (!response.ok) {
    throw Error(`${response.status}: ${response.statusText}`);
  }
  return response;
}

async function _fetch(url: string, onSuccess: Function, onFailure: Function, options: any = {}, expectResponse = true) {
  return fetch(url, options)
    .then(_handleErrors)
    .then(res => expectResponse ? res.json() : onSuccess())
    .then(json => onSuccess(json))
    .catch(err => { 
      onFailure(err.message)
    })
}

async function _get(url: string, onSuccess: Function, onFailure: Function, withAuth = false) {
  const options: any = {}
  if (withAuth) {
    const token = await getIdToken()
    options.headers = {
      'Authorization': token
    }
  }
  return _fetch(url, onSuccess, onFailure, options)
}

async function _post(url: string, body: any, onSuccess: Function, onFailure: Function, expectResponse: boolean = true) {
  const token = await getIdToken()
  const options = {
    headers: { 
      'Content-Type': 'application/json',
      'Authorization': token
    },
    method: 'POST',
    body: JSON.stringify(body)
  }
  return _fetch(url, onSuccess, onFailure, options, expectResponse)
}

async function _delete(url: string, body: any, onSuccess: Function, onFailure: Function) {
  const token = await getIdToken()
  const options = {
    headers: { 
      'Content-Type': 'application/json',
      'Authorization': token
      },
    method: 'DELETE',
    body: JSON.stringify(body)
  }
  return _fetch(url, onSuccess, onFailure, options)
}

export async function addOrModifyEntry (endpoint: string, entry: any, onSuccess: Function, onFailure: Function) {
  const url =  `${getEndpointBase()}/api/${endpoint}`
  return _post(url, entry, onSuccess, onFailure)
}

export async function removeById (endpoint: string, id: string, onSuccess: Function, onFailure: Function) {
  const url =  `${getEndpointBase()}/api/${endpoint}`
  return _delete(url, { id }, onSuccess, onFailure)
}

export async function fetchAll (endpoint: string, onSuccess: Function, onFailure: Function) {
  let url = `${getEndpointBase()}/api/${endpoint}`
  return _get(url, onSuccess, onFailure, true)
}

export async function fetchById (endpoint: string, id: string, onSuccess: Function, onFailure: Function) {
  const url = `${getEndpointBase()}/api/${endpoint}/${id}`
  return _get(url, onSuccess, onFailure, true)
}


