import { FuzzyDetermine, Difficulty } from "constants/enums"
import { DetermineInputs } from "types/encounters"
import { thresholds } from './encounter-generator/constants'

const crToXp: { [cr: number]: number } = {
  0.125: 25,
  0.25: 50,
  0.5: 100,
  1: 200,
  2: 450,
  3: 700,
  4: 1100,
  5: 1800,
  6: 2300,
  7: 2900,
  8: 3900,
  9: 5000,
  10: 5900,
  11: 7200,
  12: 8400,
  13: 10000,
  14: 11500,
  15: 13000,
  16: 15000,
  17: 18000,
  18: 20000,
  19: 22000,
  20: 25000,
  21: 33000,
  22: 41000,
  23: 50000,
  24: 62000,
  25: 75000 
}

const monsterMultiplierTiers = [0.5, 1, 1.5, 2, 2.5, 3, 4, 5]

function getMonsterMultipler (numMonsters: number, numPcs: number): number {
  let resultTier = 0
  if (numMonsters === 1) {
    resultTier = 1 
  } else if (numMonsters === 2) {
    resultTier = 2
  } else if (numMonsters >= 3 && numMonsters <= 6) {
    resultTier = 3 
  } else if (numMonsters >= 7 && numMonsters <= 10) {
    resultTier = 4 
  } else if (numMonsters >= 11 && numMonsters <= 14) {
    resultTier = 5 
  } else if (numMonsters >= 15) {
    resultTier = 6 
  }

  if (numPcs < 3) resultTier++
  if (numPcs > 5) resultTier--

  return monsterMultiplierTiers[resultTier]
}

function _promoteLowest (monsterCrs: number[]) {
  const min = Math.min(...monsterCrs)
  const minIdx = monsterCrs.findIndex(cr => cr === min)

  return monsterCrs.map((cr, idx) => idx === minIdx ? cr + 1 : cr)
}

function _getEcounterXp (monsterCrs: number[], numPcs: number) {
  let encounterXp = 0
  for (const cr of monsterCrs) {
    encounterXp += crToXp[cr]
  }
  encounterXp *= getMonsterMultipler(monsterCrs.length, numPcs)
  return encounterXp
}

function _determineAvgLvl(difficulty?: Difficulty, monsterCrs: number[] = [], numPcs?: number) {
  if (!difficulty || !monsterCrs.length || !numPcs) return null

  const encounterXp = _getEcounterXp(monsterCrs, numPcs)

  let resultLvl = 1
  for (const xpRow of thresholds) {
    const xpThresh = xpRow[difficulty - 1] * numPcs
    if (xpThresh > encounterXp) {
      resultLvl--
      break
    } 
    resultLvl++
  }
  return Math.min(resultLvl, 20)
}

function _determineDifficulty (monsterCrs: number[] = [], numPcs?: number, avgLvl?: number) {
  if (!avgLvl || !monsterCrs.length || !numPcs) return null

  const encounterXp = _getEcounterXp(monsterCrs, numPcs)

  let difficulty = Difficulty.Easy
  const thresholdRow = thresholds[avgLvl - 1]
  for (const baseXp of thresholdRow) {
    const xpThresh = baseXp * numPcs
    if (encounterXp < xpThresh) {
      difficulty--
      break
    }
    difficulty++
  }
  return Math.max(Math.min(difficulty, Difficulty.Deadly), Difficulty.Easy)
}

function _determineNumPlayers (monsterCrs: number[] = [], avgLvl?: number, difficulty?: Difficulty) {
  if (!avgLvl || !monsterCrs.length || !difficulty) return null

  const encounterXpSmall = _getEcounterXp(monsterCrs, 2)
  const encounterXpNormal = _getEcounterXp(monsterCrs, 4)
  const encounterXpLarge = _getEcounterXp(monsterCrs, 6)

  const tableXp = thresholds[avgLvl - 1][difficulty - 1]

  const numPcsSmall = Math.round(encounterXpSmall / tableXp)
  const numPcsNormal = Math.round(encounterXpNormal / tableXp)
  const numPcsLarge = Math.round(encounterXpLarge / tableXp)

  let result
  if (numPcsNormal > 5 && numPcsLarge > 5) {
    result = numPcsLarge
  } else if (numPcsNormal < 3 && numPcsSmall < 3) {
    result = numPcsSmall
  } else {
    result = numPcsNormal
  }

  return result
}

function _determineNumMonsters (monsterCr?: number, numPcs?: number, avgLvl?: number, difficulty?: Difficulty) {
  if (!avgLvl || !monsterCr || !difficulty || !numPcs) return null

  const xpThresh = thresholds[avgLvl - 1][difficulty - 1] * numPcs

  let numMonsters = 1
  let keepGoing = true
  while (keepGoing) {
    const monsterCrs = []
    for (let i = 0; i < numMonsters; i++) {
      monsterCrs.push(monsterCr)
    }

    const encounterXp = _getEcounterXp(monsterCrs, numPcs)

    if (encounterXp > xpThresh) {
      numMonsters--
      keepGoing = false
    } else {
      numMonsters++
    }
  }
  return numMonsters
}

function _determineMonsterCrs (numPcs?: number, numMonsters?: number, avgLvl?: number, difficulty?: Difficulty) {
  if (!avgLvl || !numMonsters || !difficulty || !numPcs) return null

  const xpThresh = thresholds[avgLvl - 1][difficulty - 1] * numPcs
  const monsterMult = getMonsterMultipler(numMonsters, numPcs)

  const averageMonsterXp = xpThresh / numMonsters / monsterMult
  let monsterCrLowerBound = 0.125 
  for (const [cr, xp] of Object.entries(crToXp)) {
    if (xp < averageMonsterXp) {
      monsterCrLowerBound = parseFloat(cr)
    } else {
      break
    }
  }

  let currMonsterCrs = []
  for (let i = 0; i < numMonsters; i++) currMonsterCrs.push(monsterCrLowerBound) 

  let keepGoing = true
  while(keepGoing) {
    const attemptedPromotion = _promoteLowest(currMonsterCrs)
    if (_getEcounterXp(attemptedPromotion, numPcs) <= xpThresh) {
      currMonsterCrs = attemptedPromotion
    } else {
      keepGoing = false
    }
  }

  return currMonsterCrs
}

export function fuzzyEncounterCalculate (determine: FuzzyDetermine, inputs: DetermineInputs) {
  const { difficulty, monsterCrs, monsterCr, numPcs, avgLvl, numMonsters } = inputs

  let result
  switch (determine) {
    case FuzzyDetermine.AvgLvl:
      result = _determineAvgLvl(difficulty, monsterCrs, numPcs) 
      break
    case FuzzyDetermine.Difficulty:
      result = _determineDifficulty(monsterCrs, numPcs, avgLvl)
      break
    case FuzzyDetermine.NumPlayers:
      result = _determineNumPlayers(monsterCrs, avgLvl, difficulty)
      break
    case FuzzyDetermine.NumMonsters:
      result = _determineNumMonsters(monsterCr, numPcs, avgLvl, difficulty)
      break
    case FuzzyDetermine.MonsterCr:
      result = _determineMonsterCrs(numPcs, numMonsters, avgLvl, difficulty)
      break
    default:
      break
  }
  
  return typeof result !== 'undefined' ? result : null
}
