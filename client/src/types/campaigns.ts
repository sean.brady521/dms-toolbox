export interface ICampaign {
  id: string
  name?: string
  createdBy: string
  description?: string
}