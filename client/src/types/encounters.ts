import { Environment, Difficulty } from "constants/enums";

export interface IEncounter {
  creatures: string
  personality: string
  relationship: string
}

export interface EncounterConfig {
  avgLvl: number
  numPcs: number
  environment: Environment
  difficulty: Difficulty
}

export interface EncounterRow extends IEncounter {
  liklihood: number
}

export interface EncounterTableBase {
  id: string
  name: string
  rows: EncounterRow[]
}

export interface EncounterTable extends EncounterTableBase {
  modifiedAt: number
  createdBy: string
  createdAt: number
}

export interface Personality {
  type: string
  description: string
}

export interface Relationship {
  type: string
  description: string
  joiner: string
}

export interface DetermineInputs {
  numPcs?: number
  avgLvl?: number
  numMonsters?: number
  monsterCrs?: number[]
  monsterCr?: number
  difficulty?: Difficulty

}