import { INpc } from "./npcs";
import { ICampaign } from "./campaigns";
import { IEncounter, EncounterTable } from "./encounters";

export interface StoreState {
  router: RouterState
  user: UserState
  npcs: NpcsState
  campaigns: CampaignsState
  encounters: EncountersState
}

export interface RouterState {
  location: Location
  action: string
}

export interface Location {
  pathname: string
  search: string
  hash: string
}

export interface NpcsState {
  npcs: INpc[]
  error: string | null
  loading: boolean
  selectedCampaign: string
  search: string
  sortConfig: SortConfig
}

export interface CampaignsState {
  campaigns: ICampaign[]
  error: string | null
  loading: boolean
}

export interface EncountersState {
  tables: EncounterTable[]
  error: string | null
  loading: boolean
}


export interface UserState {
  loading: boolean
  loggedIn: boolean
  error: string | null
}

export interface Option<T> {
  value: T
  label: string
}

export interface SortConfig {
  field: string
  direction: 'asc' | 'desc'
}

export type ValueOf<T> = T[keyof T] 