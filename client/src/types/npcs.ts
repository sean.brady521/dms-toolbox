import { Npc } from 'utils/npc-generator/npc-data'

export interface Voice {
  pace?: string
  pitch?: string
  tone?: string
  speechQuirk?: string
  physicalQuirk?: string
}

export interface GeneratedNpc extends Npc {}

export interface GenerateOptions {
  race?: number | null
  plothook?: number | null
  classorprof?: number | null
  occupation1?: number | null
  occupation2?: number | null
  gender?: number | null
}

export interface INpc {
  id: string
  createdBy: string
  modifiedAt: number
  createdAt: number
  campaign: string
  imageUrl: string
  name?: string
  context?: string
  description?: string
  hook?: string
  personality?: string
  gender?: string
  race?: string
  class?: string
  voice?: Voice
  abilities?: NpcAbilities
  alignment?: string
}

export interface Traits {
  traits1: string
  traits2: string
}

export interface NpcGeneraterConfig {
  autoRandomise?: boolean
}

export interface NpcAbilities {
  str: number,
  dex: number,
  con: number,
  int: number,
  wis: number,
  cha: number
}